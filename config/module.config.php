<?php

namespace Lerp\Document;

use Laminas\I18n\Translator\Loader\PhpArray;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Document\Controller\Ajax\DocStreamController;
use Lerp\Document\Controller\Ajax\DoctItemController;
use Lerp\Document\Controller\Ajax\Factoryorder\FactoryorderController;
use Lerp\Document\Controller\Ajax\Offer\OfferController;
use Lerp\Document\Controller\Ajax\Order\OrderController;
use Lerp\Document\Controller\Ajax\Purchase\PurchaseController;
use Lerp\Document\Controller\Rest\Factoryorder\FactoryorderRestController;
use Lerp\Document\Controller\Rest\Offer\OfferRestController;
use Lerp\Document\Controller\Rest\Order\DeliveryRestController;
use Lerp\Document\Controller\Rest\Order\EstimateRestController;
use Lerp\Document\Controller\Rest\Order\InvoiceRestController;
use Lerp\Document\Controller\Rest\Order\OrderConfirmRestController;
use Lerp\Document\Controller\Rest\Order\ProformaRestController;
use Lerp\Document\Controller\Rest\Purchase\PurchaseDeliverRestController;
use Lerp\Document\Controller\Rest\Purchase\PurchaseOrderRestController;
use Lerp\Document\Controller\Rest\Purchase\PurchaseRequestRestController;
use Lerp\Document\Factory\Controller\Ajax\DocStreamControllerFactory;
use Lerp\Document\Factory\Controller\Ajax\DoctItemControllerFactory;
use Lerp\Document\Factory\Controller\Ajax\Factoryorder\FactoryorderControllerFactory;
use Lerp\Document\Factory\Controller\Ajax\Offer\OfferControllerFactory;
use Lerp\Document\Factory\Controller\Ajax\Order\OrderControllerFactory;
use Lerp\Document\Factory\Controller\Ajax\Purchase\PurchaseControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Factoryorder\FactoryorderRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Offer\OfferRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Order\DeliveryRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Order\EstimateRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Order\InvoiceRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Order\OrderConfirmRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Order\ProformaRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Purchase\PurchaseDeliverRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Purchase\PurchaseOrderRestControllerFactory;
use Lerp\Document\Factory\Controller\Rest\Purchase\PurchaseRequestRestControllerFactory;
use Lerp\Document\Factory\DocumentTranslatorFactory;
use Lerp\Document\Factory\Form\DocCreateFormFactory;
use Lerp\Document\Factory\Service\Create\Factoryorder\CreateFactoryorderServiceFactory;
use Lerp\Document\Factory\Service\Create\Offer\CreateOfferServiceFactory;
use Lerp\Document\Factory\Service\Create\Order\CreateDeliveryServiceFactory;
use Lerp\Document\Factory\Service\Create\Order\CreateEstimateServiceFactory;
use Lerp\Document\Factory\Service\Create\Order\CreateInvoiceServiceFactory;
use Lerp\Document\Factory\Service\Create\Order\CreateOrderConfirmServiceFactory;
use Lerp\Document\Factory\Service\Create\Order\CreateProformaServiceFactory;
use Lerp\Document\Factory\Service\Create\Purchase\CreatePurchaseDeliverServiceFactory;
use Lerp\Document\Factory\Service\Create\Purchase\CreatePurchaseOrderServiceFactory;
use Lerp\Document\Factory\Service\Create\Purchase\CreatePurchaseRequestServiceFactory;
use Lerp\Document\Factory\Service\DocumentGodServiceFactory;
use Lerp\Document\Factory\Service\DocumentServiceFactory;
use Lerp\Document\Factory\Service\OfferDocumentServiceFactory;
use Lerp\Document\Factory\Service\OrderDocumentServiceFactory;
use Lerp\Document\Factory\Service\PurchaseDocumentServiceFactory;
use Lerp\Document\Factory\Service\FactoryorderDocumentServiceFactory;
use Lerp\Document\Factory\Service\Stream\DocStreamServiceFactory;
use Lerp\Document\Factory\Table\Factoryorder\DocFactoryorderItemTableFactory;
use Lerp\Document\Factory\Table\Factoryorder\DocFactoryorderTableFactory;
use Lerp\Document\Factory\Table\Offer\DocOfferItemTableFactory;
use Lerp\Document\Factory\Table\Offer\DocOfferSendTableFactory;
use Lerp\Document\Factory\Table\Offer\DocOfferTableFactory;
use Lerp\Document\Factory\Table\Order\DocDeliveryItemTableFactory;
use Lerp\Document\Factory\Table\Order\DocDeliverySendTableFactory;
use Lerp\Document\Factory\Table\Order\DocDeliveryTableFactory;
use Lerp\Document\Factory\Table\Order\DocEstimateItemTableFactory;
use Lerp\Document\Factory\Table\Order\DocEstimateSendTableFactory;
use Lerp\Document\Factory\Table\Order\DocEstimateTableFactory;
use Lerp\Document\Factory\Table\Order\DocInvoiceItemTableFactory;
use Lerp\Document\Factory\Table\Order\DocInvoiceSendTableFactory;
use Lerp\Document\Factory\Table\Order\DocInvoiceTableFactory;
use Lerp\Document\Factory\Table\Order\DocOrderConfirmItemTableFactory;
use Lerp\Document\Factory\Table\Order\DocOrderConfirmSendTableFactory;
use Lerp\Document\Factory\Table\Order\DocOrderConfirmTableFactory;
use Lerp\Document\Factory\Table\Order\DocProformaItemTableFactory;
use Lerp\Document\Factory\Table\Order\DocProformaSendTableFactory;
use Lerp\Document\Factory\Table\Order\DocProformaTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseDeliverItemTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseDeliverSendTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseDeliverTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseOrderItemTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseOrderSendTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseOrderTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseRequestItemTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseRequestSendTableFactory;
use Lerp\Document\Factory\Table\Purchase\DocPurchaseRequestTableFactory;
use Lerp\Document\Form\DocCreateForm;
use Lerp\Document\Service\Create\Factoryorder\CreateFactoryorderService;
use Lerp\Document\Service\Create\Offer\CreateOfferService;
use Lerp\Document\Service\Create\Order\CreateDeliveryService;
use Lerp\Document\Service\Create\Order\CreateEstimateService;
use Lerp\Document\Service\Create\Order\CreateInvoiceService;
use Lerp\Document\Service\Create\Order\CreateOrderConfirmService;
use Lerp\Document\Service\Create\Order\CreateProformaService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseDeliverService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseOrderService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseRequestService;
use Lerp\Document\Service\DocumentGodService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Service\Stream\DocStreamService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Table\Order\DocDeliveryItemTable;
use Lerp\Document\Table\Order\DocDeliverySendTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateItemTable;
use Lerp\Document\Table\Order\DocEstimateSendTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceItemTable;
use Lerp\Document\Table\Order\DocInvoiceSendTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmSendTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaItemTable;
use Lerp\Document\Table\Order\DocProformaSendTable;
use Lerp\Document\Table\Order\DocProformaTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;

return [
    'router'          => [
        'routes' => [
            // AJAX - create purchase order
            'lerp_document_purchaseorder_create'             => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-purchaseorder-create',
                    'defaults' => [
                        'controller' => PurchaseController::class,
                        'action'     => 'createPurchaseOrder'
                    ],
                ],
            ],
            // AJAX - create purchase deliver
            'lerp_document_purchasedeliver_create'           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-purchasedeliver-create',
                    'defaults' => [
                        'controller' => PurchaseController::class,
                        'action'     => 'createPurchaseDeliver'
                    ],
                ],
            ],
            // AJAX - create purchase request
            'lerp_document_purchaserequest_create'           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-purchaserequest-create',
                    'defaults' => [
                        'controller' => PurchaseController::class,
                        'action'     => 'createPurchaseRequest'
                    ],
                ],
            ],
            // AJAX - create offer
            'lerp_document_offer_create_offer'               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-offer-create',
                    'defaults' => [
                        'controller' => OfferController::class,
                        'action'     => 'createOffer'
                    ],
                ],
            ],
            // AJAX - create order
            'lerp_document_order_create_estimate'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-estimate-create',
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action'     => 'createEstimate'
                    ],
                ],
            ],
            'lerp_document_order_create_orderconfirm'        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-orderconfirm-create',
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action'     => 'createOrderConfirm'
                    ],
                ],
            ],
            'lerp_document_order_create_delivery'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-delivery-create',
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action'     => 'createDelivery'
                    ],
                ],
            ],
            'lerp_document_order_create_invoice'             => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-invoice-create',
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action'     => 'createInvoice'
                    ],
                ],
            ],
            'lerp_document_order_create_proforma'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-proforma-create',
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action'     => 'createProforma'
                    ],
                ],
            ],
            // AJAX - create factoryorder
            'lerp_document_factoryorder_create_factoryorder' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-document-factoryorder-create',
                    'defaults' => [
                        'controller' => FactoryorderController::class,
                        'action'     => 'createFactoryorder'
                    ],
                ],
            ],
            // AJAX - stream
            'lerp_document_ajax_docstream_estimate'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-estimate[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamEstimate'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_offer'                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-offer[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamOffer'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_orderconfirm'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-orderconfirm[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamOrderConfirm'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_delivery'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-delivery[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamDelivery'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_proforma'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-proforma[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamProforma'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_invoice'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-invoice[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamInvoice'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_purchaseorder'          => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-purchaseorder[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamPurchaseOrder'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_purchasedeliver'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-purchasedeliver[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamPurchaseDeliver'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_purchaserequest'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-purchaserequest[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamPurchaseRequest'
                    ],
                ],
            ],
            'lerp_document_ajax_docstream_factoryorder'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-stream-factoryorder[/:uuid]',
                    'constraints' => [
                        'uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DocStreamController::class,
                        'action'     => 'streamFactoryorder'
                    ],
                ],
            ],
            // AJAX - doc item
            'lerp_document_ajax_docitem_purchaseorderitemproduct'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-docitem-purchase-order-item-product[/:product_uuid]',
                    'constraints' => [
                        'product_uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DoctItemController::class,
                        'action'     => 'purchaseOrderItemsProduct'
                    ],
                ],
            ],
            'lerp_document_ajax_docitem_invoiceitemproduct'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-docitem-invoice-item-product[/:product_uuid]',
                    'constraints' => [
                        'product_uuid' => '[a-fA-F0-9-]+',
                    ],
                    'defaults'    => [
                        'controller' => DoctItemController::class,
                        'action'     => 'invoiceItemsProduct'
                    ],
                ],
            ],
            // REST
            'lerp_document_rest_purchase_purchaseorder'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-purchase-purchaseorder[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_purchase_purchasedeliver'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-purchase-purchasedeliver[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseDeliverRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_purchase_purchaserequest'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-purchase-purchaserequest[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_offer_offer'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-offer-offer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OfferRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_order_estimate'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-order-estimate[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EstimateRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_order_orderconfirm'               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-order-orderconfirm[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OrderConfirmRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_order_delivery'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-order-delivery[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => DeliveryRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_order_invoice'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-order-invoice[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => InvoiceRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_order_proforma'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-order-proforma[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProformaRestController::class,
                    ],
                ],
            ],
            'lerp_document_rest_factoryorder_factoryorder'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-document-rest-factoryorder-factoryorder[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FactoryorderRestController::class,
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            // purchase-order
            PurchaseController::class            => PurchaseControllerFactory::class,
            PurchaseOrderRestController::class   => PurchaseOrderRestControllerFactory::class,
            PurchaseDeliverRestController::class => PurchaseDeliverRestControllerFactory::class,
            PurchaseRequestRestController::class => PurchaseRequestRestControllerFactory::class,
            // offer
            OfferRestController::class           => OfferRestControllerFactory::class,
            OfferController::class               => OfferControllerFactory::class,
            // order
            OrderController::class               => OrderControllerFactory::class,
            EstimateRestController::class        => EstimateRestControllerFactory::class,
            OrderConfirmRestController::class    => OrderConfirmRestControllerFactory::class,
            DeliveryRestController::class        => DeliveryRestControllerFactory::class,
            InvoiceRestController::class         => InvoiceRestControllerFactory::class,
            ProformaRestController::class        => ProformaRestControllerFactory::class,
            // factoryorder
            FactoryorderController::class        => FactoryorderControllerFactory::class,
            FactoryorderRestController::class    => FactoryorderRestControllerFactory::class,
            //
            DocStreamController::class           => DocStreamControllerFactory::class,
            DoctItemController::class            => DoctItemControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            DocumentGodService::class           => DocumentGodServiceFactory::class,
            DocumentService::class              => DocumentServiceFactory::class,
            DocStreamService::class             => DocStreamServiceFactory::class,
            // service - offer
            OfferDocumentService::class         => OfferDocumentServiceFactory::class,
            CreateOfferService::class           => CreateOfferServiceFactory::class,
            // service - order
            OrderDocumentService::class         => OrderDocumentServiceFactory::class,
            CreateEstimateService::class        => CreateEstimateServiceFactory::class,
            CreateOrderConfirmService::class    => CreateOrderConfirmServiceFactory::class,
            CreateDeliveryService::class        => CreateDeliveryServiceFactory::class,
            CreateInvoiceService::class         => CreateInvoiceServiceFactory::class,
            CreateProformaService::class        => CreateProformaServiceFactory::class,
            // service - purchase
            PurchaseDocumentService::class      => PurchaseDocumentServiceFactory::class,
            CreatePurchaseOrderService::class   => CreatePurchaseOrderServiceFactory::class,
            CreatePurchaseDeliverService::class => CreatePurchaseDeliverServiceFactory::class,
            CreatePurchaseRequestService::class => CreatePurchaseRequestServiceFactory::class,
            // service - factoryorder
            FactoryorderDocumentService::class  => FactoryorderDocumentServiceFactory::class,
            CreateFactoryorderService::class    => CreateFactoryorderServiceFactory::class,
            // table - offer
            DocOfferTable::class                => DocOfferTableFactory::class,
            DocOfferItemTable::class            => DocOfferItemTableFactory::class,
            DocOfferSendTable::class            => DocOfferSendTableFactory::class,
            // table - order
            DocEstimateTable::class             => DocEstimateTableFactory::class,
            DocEstimateItemTable::class         => DocEstimateItemTableFactory::class,
            DocEstimateSendTable::class         => DocEstimateSendTableFactory::class,
            DocInvoiceTable::class              => DocInvoiceTableFactory::class,
            DocInvoiceItemTable::class          => DocInvoiceItemTableFactory::class,
            DocInvoiceSendTable::class          => DocInvoiceSendTableFactory::class,
            DocOrderConfirmTable::class         => DocOrderConfirmTableFactory::class,
            DocOrderConfirmItemTable::class     => DocOrderConfirmItemTableFactory::class,
            DocOrderConfirmSendTable::class     => DocOrderConfirmSendTableFactory::class,
            DocDeliveryTable::class             => DocDeliveryTableFactory::class,
            DocDeliveryItemTable::class         => DocDeliveryItemTableFactory::class,
            DocDeliverySendTable::class         => DocDeliverySendTableFactory::class,
            DocProformaTable::class             => DocProformaTableFactory::class,
            DocProformaItemTable::class         => DocProformaItemTableFactory::class,
            DocProformaSendTable::class         => DocProformaSendTableFactory::class,
            // table - purchase
            DocPurchaseOrderTable::class        => DocPurchaseOrderTableFactory::class,
            DocPurchaseOrderItemTable::class    => DocPurchaseOrderItemTableFactory::class,
            DocPurchaseOrderSendTable::class    => DocPurchaseOrderSendTableFactory::class,
            DocPurchaseDeliverTable::class      => DocPurchaseDeliverTableFactory::class,
            DocPurchaseDeliverItemTable::class  => DocPurchaseDeliverItemTableFactory::class,
            DocPurchaseDeliverSendTable::class  => DocPurchaseDeliverSendTableFactory::class,
            DocPurchaseRequestTable::class      => DocPurchaseRequestTableFactory::class,
            DocPurchaseRequestItemTable::class  => DocPurchaseRequestItemTableFactory::class,
            DocPurchaseRequestSendTable::class  => DocPurchaseRequestSendTableFactory::class,
            // table - factoryorder
            DocFactoryorderTable::class         => DocFactoryorderTableFactory::class,
            DocFactoryorderItemTable::class     => DocFactoryorderItemTableFactory::class,
            // form
            DocCreateForm::class                => DocCreateFormFactory::class,
            // doc translator
            'DocumentTranslator'                => DocumentTranslatorFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_document'   => [
        'path_pdf'   => __DIR__ . '/../../../../data/files/documents/pdf',
        'translator' => [
            'locale'                    => ['de_DE.utf8', 'en_GB.utf8'],
            'translation_file_patterns' => [
                [
                    'type'        => PhpArray::class,
                    'base_dir'    => __DIR__ . '/../language',
                    'pattern'     => '%s.php',
                    'text_domain' => 'lerp_doc'
                ],
            ],
        ],
    ],
];
