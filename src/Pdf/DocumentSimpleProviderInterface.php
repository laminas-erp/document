<?php

namespace Lerp\Document\Pdf;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Service\DocumentService;

/**
 * Interface DocumentInfoProviderInterface
 * @package Lerp\Document\Pdf
 * This ProviderInterface is for all PdfDocuments wich create a PDF document only for informations - mostly internal.
 * ...e.g. for factoryorder.
 */
interface DocumentSimpleProviderInterface
{
    public function setDocumentService(DocumentService $documentService): void;

    /**
     * @param Translator $documentTranslator ServiceManager key 'DocumentTranslator' ...or your own configured translator.
     */
    public function setDocumentTranslator(Translator $documentTranslator): void;

    public function setNumberFormatService(NumberFormatService $numberFormatService): void;

    public function setLangIso(string $langIso): void;

    public function makeDocument(): void;

    public function getPdfFilename(): string;

    /**
     * @return string Path of the created PDF document
     */
    public function writeDocument(): string;
}
