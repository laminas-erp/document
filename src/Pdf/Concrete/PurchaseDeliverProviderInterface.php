<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Purchase\Entity\PurchaseOrderItemAttachEntity;

interface PurchaseDeliverProviderInterface
{
    /**
     * @param PurchaseOrderItemAttachEntity[] $purchaseOrderItemAttachEntities
     */
    public function setPurchaseOrderItemAttachEntities(array $purchaseOrderItemAttachEntities): void;

    /**
     * @return PurchaseOrderItemAttachEntity[]
     */
    public function getPurchaseOrderItemAttachEntities(): array;

    public function setPurchaseOrderNoCompl(string $purchaseOrderNoCompl): void;

    public function setDocPurchaseDeliverNoCompl(string $docPurchaseDeliverNoCompl): void;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;

}
