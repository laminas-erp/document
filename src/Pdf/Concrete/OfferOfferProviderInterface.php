<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Order\Entity\Offer\OfferItemEntity;

/**
 * Interface OfferOfferProviderInterface
 * @package Lerp\Document\Pdf
 * Providing doc-offer in offer.
 */
interface OfferOfferProviderInterface
{

    public function setDocOfferNoCompl(string $docOfferNoCompl): void;

    /**
     * @param OfferItemEntity[] $offerItemEntities
     */
    public function setOfferItemEntities(array $offerItemEntities): void;

    /**
     * @return OfferItemEntity[]
     */
    public function getOfferItemEntities(): array;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;
}
