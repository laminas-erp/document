<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Order\Entity\Order\OrderItemEntity;

interface InvoiceProviderInterface
{

    public function setDocInvoiceNoCompl(string $docInvoiceNoCompl): void;

    /**
     * @param string $invoiceType invoice, coupon, partial
     */
    public function setInvoiceType(string $invoiceType): void;

    /**
     * @param OrderItemEntity[] $orderItemEntities
     */
    public function setOrderItemEntities(array $orderItemEntities): void;

    /**
     * @return OrderItemEntity[]
     */
    public function getOrderItemEntities(): array;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;
}
