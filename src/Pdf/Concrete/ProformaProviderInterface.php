<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Order\Entity\Order\OrderItemEntity;

interface ProformaProviderInterface
{

    public function setDocProformaNoCompl(string $docProformaNoCompl): void;

    /**
     * @param OrderItemEntity[] $orderItemEntities
     */
    public function setOrderItemEntities(array $orderItemEntities): void;

    /**
     * @return OrderItemEntity[]
     */
    public function getOrderItemEntities(): array;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;
}
