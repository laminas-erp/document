<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Entity\FactoryorderWorkflowEntity;

interface FactoryorderProviderInterface
{
    public function setFactoryOrder(FactoryorderEntity $factoryOrder): void;

    /**
     * @param FactoryorderWorkflowEntity[] $factoryorderWorkflows
     */
    public function setFactoryorderWorkflows(array $factoryorderWorkflows): void;
}
