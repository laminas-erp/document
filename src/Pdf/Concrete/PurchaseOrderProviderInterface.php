<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Purchase\Entity\PurchaseOrderItemEntity;

interface PurchaseOrderProviderInterface
{
    /**
     * @param PurchaseOrderItemEntity[] $purchaseOrderItemEntities
     */
    public function setPurchaseOrderItemEntities(array $purchaseOrderItemEntities): void;

    /**
     * @return PurchaseOrderItemEntity[]
     */
    public function getPurchaseOrderItemEntities(): array;

    public function setPurchaseOrderNoCompl(string $purchaseOrderNoCompl): void;

    public function setDocPurchaseOrderNoCompl(string $docPurchaseOrderNoCompl): void;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;
}
