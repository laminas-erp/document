<?php

namespace Lerp\Document\Pdf\Concrete;

use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Purchase\Entity\PurchaseRequestItemEntity;

interface PurchaseRequestProviderInterface
{
    /**
     * @param PurchaseRequestItemEntity[] $purchaseRequestItemEntities
     */
    public function setPurchaseRequestItemEntities(array $purchaseRequestItemEntities): void;

    /**
     * @return PurchaseRequestItemEntity[]
     */
    public function getPurchaseRequestItemEntities(): array;

    public function setPurchaseRequestNoCompl(string $purchaseRequestNoCompl): void;

    public function setDocPurchaseRequestNoCompl(string $docPurchaseRequestNoCompl): void;

    public function setBaseDataEntity(BaseDataEntity $baseDataEntity): void;
}
