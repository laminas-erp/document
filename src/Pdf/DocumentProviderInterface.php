<?php

namespace Lerp\Document\Pdf;

use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Service\DocumentService;

/**
 * Interface DocumentProviderInterface
 * @package Lerp\Document\Pdf
 * This ProviderInterface is for all PdfDocuments wich create a PDF document for customers.
 */
interface DocumentProviderInterface
{
    public function setDocumentService(DocumentService $documentService): void;

    /**
     * @param Translator $documentTranslator ServiceManager key 'DocumentTranslator' ...or your own configured translator.
     */
    public function setDocumentTranslator(Translator $documentTranslator): void;

    public function setNumberFormatService(NumberFormatService $numberFormatService): void;

    public function getPdfFilename(): string;

    public function getPriceSum(): float;

    public function getTaxSum(): float;

    public function makeDocument(): void;

    /**
     * @return string Path of the created PDF document
     */
    public function writeDocument(): string;
}
