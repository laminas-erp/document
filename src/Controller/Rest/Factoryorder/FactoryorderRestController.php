<?php

namespace Lerp\Document\Controller\Rest\Factoryorder;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\FactoryorderDocumentService;

class FactoryorderRestController extends AbstractUserRestController
{
    protected DocumentService $documentService;
    protected FactoryorderDocumentService $factoryorderDocumentService;

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setFactoryorderDocumentService(FactoryorderDocumentService $factoryorderDocumentService): void
    {
        $this->factoryorderDocumentService = $factoryorderDocumentService;
    }

    /**
     * GET doc_factoryorder for factoryorder_uuid - only one doc per factoryorder.
     * @param string $id factoryorder_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($doc = $this->factoryorderDocumentService->getDocFactoryorderByFactoryorderUuid($id))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setObj($doc);
        }
        return $jsonModel;
    }

    /**
     * DELETE a complete (with * doc_factoryorder_item) doc_factoryorder.
     * @param string $id doc_factoryorder_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if(!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if($this->factoryorderDocumentService->deleteDocFactoryorderComplete($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

	/**
	 * POST maps to create().
	 * @param array $data
	 * @return JsonModel
	 */
	public function create($data): JsonModel
	{
		$jsonModel = new JsonModel();
		$this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
		return $jsonModel;
	}

	/**
	 * PUT maps to update().
	 * @param string $id
	 * @param array $data
	 * @return JsonModel
	 */
	public function update($id, $data): JsonModel
	{
		$jsonModel = new JsonModel();
		$this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
		return $jsonModel;
	}
}
