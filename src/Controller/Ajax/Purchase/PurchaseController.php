<?php

namespace Lerp\Document\Controller\Ajax\Purchase;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseDeliverService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseOrderService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseRequestService;
use Laminas\Http\Response;

class PurchaseController extends AbstractUserController
{
    protected CreatePurchaseOrderService $createPurchaseOrderService;
    protected CreatePurchaseDeliverService $createPurchaseDeliverService;
    protected CreatePurchaseRequestService $createPurchaseRequestService;

    public function setCreatePurchaseOrderService(CreatePurchaseOrderService $createPurchaseOrderService): void
    {
        $this->createPurchaseOrderService = $createPurchaseOrderService;
    }

    public function setCreatePurchaseDeliverService(CreatePurchaseDeliverService $createPurchaseDeliverService): void
    {
        $this->createPurchaseDeliverService = $createPurchaseDeliverService;
    }

    public function setCreatePurchaseRequestService(CreatePurchaseRequestService $createPurchaseRequestService): void
    {
        $this->createPurchaseRequestService = $createPurchaseRequestService;
    }

    /**
     * @return JsonModel
     */
    public function createPurchaseOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_*_uuid
        $uuidsArr = json_decode($this->getRequest()->getPost('uuids'), true);
        $start = filter_input(INPUT_POST, 'start', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $foot = filter_input(INPUT_POST, 'foot', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $validator = new Uuid();
        if (empty($uuid) || !$validator->isValid($uuid) || empty($uuidsArr) || !is_array($uuidsArr) || (!empty($uuidRepl) && !$validator->isValid($uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            if (empty($uuidsArr)) {
                $jsonModel->addMessage('Es sind keine Items ausgewählt.');
            }
            return $jsonModel;
        }
        $uuids = [];
        foreach ($uuidsArr as $k => $v) {
            if ($validator->isValid($k) && is_numeric($v)) {
                $uuids[$k] = $v;
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
        }
        $filePath = $this->createPurchaseOrderService->createPurchaseOrder($uuid, $uuids, $start, $foot, $this->userService->getUserUuid(), $uuidRepl);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createPurchaseOrderService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createPurchaseDeliverAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_*_uuid
        $uuidsArr = json_decode($this->getRequest()->getPost('uuids'), true);
        $start = filter_input(INPUT_POST, 'start', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $foot = filter_input(INPUT_POST, 'foot', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $validator = new Uuid();
        if (empty($uuid) || !$validator->isValid($uuid) || empty($uuidsArr) || !is_array($uuidsArr) || (!empty($uuidRepl) && !$validator->isValid($uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            if (empty($uuidsArr)) {
                $jsonModel->addMessage('Es sind keine Items ausgewählt.');
            }
            return $jsonModel;
        }
        $uuids = [];
        foreach ($uuidsArr as $k => $v) {
            if ($validator->isValid($k) && is_numeric($v)) {
                $uuids[$k] = $v;
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
        }
        $filePath = $this->createPurchaseDeliverService->createPurchaseDeliver($uuid, $uuids, $start, $foot, $this->userService->getUserUuid(), $uuidRepl);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createPurchaseOrderService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createPurchaseRequestAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_*_uuid
        $uuidsArr = json_decode($this->getRequest()->getPost('uuids'), true);
        $start = filter_input(INPUT_POST, 'start', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $foot = filter_input(INPUT_POST, 'foot', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $validator = new Uuid();
        if (empty($uuid) || !$validator->isValid($uuid) || empty($uuidsArr) || !is_array($uuidsArr) || (!empty($uuidRepl) && !$validator->isValid($uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            if (empty($uuidsArr)) {
                $jsonModel->addMessage('Es sind keine Items ausgewählt.');
            }
            return $jsonModel;
        }
        $uuids = [];
        foreach ($uuidsArr as $k => $v) {
            if ($validator->isValid($k) && is_numeric($v)) {
                $uuids[$k] = $v;
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
        }
        $filePath = $this->createPurchaseRequestService->createPurchaseRequest($uuid, $uuids, $start, $foot, $this->userService->getUserUuid(), $uuidRepl);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createPurchaseRequestService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
