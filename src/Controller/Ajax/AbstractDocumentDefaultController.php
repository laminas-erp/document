<?php

namespace Lerp\Document\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

abstract class AbstractDocumentDefaultController extends AbstractUserController
{
    protected string $uuid;
    protected string $uuidRepl;
    protected array $uuids;
    protected string $start;
    protected string $foot;
    protected bool $ignorePaidDeliv;
    protected Uuid $uuidValid;

    protected function computeInputDocumentDefault(JsonModel $jsonModel): bool
    {
        $this->uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // order_uuid
        $this->uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_invoice_uuid
        $uuidsArr = json_decode($this->getRequest()->getPost('uuids'), true); // order_item_uuid
        $this->start = filter_input(INPUT_POST, 'start', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $this->foot = filter_input(INPUT_POST, 'foot', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $this->ignorePaidDeliv = filter_input(INPUT_POST, 'ignore_paid_delivered', FILTER_VALIDATE_BOOL);
        $this->uuidValid = new Uuid();
        if (empty($this->uuid) || !$this->uuidValid->isValid($this->uuid) || empty($uuidsArr) || !is_array($uuidsArr)
            || (!empty($this->uuidRepl) && !$this->uuidValid->isValid($this->uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            if (empty($uuidsArr)) {
                $jsonModel->addMessage('Es sind keine Items ausgewählt.');
            }
            return false;
        }
        $this->uuids = [];
        foreach ($uuidsArr as $k => $v) {
            if ($this->uuidValid->isValid($k) && is_numeric($v)) {
                $this->uuids[$k] = $v;
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return false;
            }
        }
        return true;
    }
}
