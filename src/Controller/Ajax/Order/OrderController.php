<?php

namespace Lerp\Document\Controller\Ajax\Order;

use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\Response;
use Lerp\Document\Controller\Ajax\AbstractDocumentDefaultController;
use Lerp\Document\Service\Create\Order\CreateDeliveryService;
use Lerp\Document\Service\Create\Order\CreateEstimateService;
use Lerp\Document\Service\Create\Order\CreateInvoiceService;
use Lerp\Document\Service\Create\Order\CreateOrderConfirmService;
use Lerp\Document\Service\Create\Order\CreateProformaService;

class OrderController extends AbstractDocumentDefaultController
{
    protected CreateEstimateService $createEstimateService;
    protected CreateOrderConfirmService $createOrderConfirmService;
    protected CreateDeliveryService $createDeliveryService;
    protected CreateInvoiceService $createInvoiceService;
    protected CreateProformaService $createProformaService;

    public function setCreateEstimateService(CreateEstimateService $createEstimateService): void
    {
        $this->createEstimateService = $createEstimateService;
    }

    public function setCreateOrderConfirmService(CreateOrderConfirmService $createOrderConfirmService): void
    {
        $this->createOrderConfirmService = $createOrderConfirmService;
    }

    public function setCreateDeliveryService(CreateDeliveryService $createDeliveryService): void
    {
        $this->createDeliveryService = $createDeliveryService;
    }

    public function setCreateInvoiceService(CreateInvoiceService $createInvoiceService): void
    {
        $this->createInvoiceService = $createInvoiceService;
    }

    public function setCreateProformaService(CreateProformaService $createProformaService): void
    {
        $this->createProformaService = $createProformaService;
    }

    /**
     * @return JsonModel
     */
    public function createEstimateAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->computeInputDocumentDefault($jsonModel)) {
            return $jsonModel;
        }
        $filePath = $this->createEstimateService->createEstimate($this->uuid, $this->uuids, $this->start, $this->foot, $this->userService->getUserUuid(), $this->uuidRepl, $this->ignorePaidDeliv);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createEstimateService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createOrderConfirmAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->computeInputDocumentDefault($jsonModel)) {
            return $jsonModel;
        }
        $filePath = $this->createOrderConfirmService->createOrderConfirm($this->uuid, $this->uuids, $this->start, $this->foot, $this->userService->getUserUuid(), $this->uuidRepl, $this->ignorePaidDeliv);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createOrderConfirmService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createDeliveryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->computeInputDocumentDefault($jsonModel)) {
            return $jsonModel;
        }
        $filePath = $this->createDeliveryService->createDelivery($this->uuid, $this->uuids, $this->start, $this->foot, $this->userService->getUserUuid(), $this->uuidRepl, $this->ignorePaidDeliv);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createDeliveryService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createInvoiceAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->computeInputDocumentDefault($jsonModel)) {
            return $jsonModel;
        }
        $type = filter_input(INPUT_POST, 'type', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // enum_invoice_type
        $filePath = $this->createInvoiceService->createInvoice($this->uuid, $this->uuids, $this->start, $this->foot, $this->userService->getUserUuid(), $this->uuidRepl, $type, $this->ignorePaidDeliv);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createInvoiceService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function createProformaAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->computeInputDocumentDefault($jsonModel)) {
            return $jsonModel;
        }
        $filePath = $this->createProformaService->createProforma($this->uuid, $this->uuids, $this->start, $this->foot, $this->userService->getUserUuid(), $this->uuidRepl, $this->ignorePaidDeliv);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createProformaService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
