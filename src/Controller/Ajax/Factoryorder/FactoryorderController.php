<?php

namespace Lerp\Document\Controller\Ajax\Factoryorder;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\Create\Factoryorder\CreateFactoryorderService;

class FactoryorderController extends AbstractUserController
{
    protected CreateFactoryorderService $createFactoryorderService;

    public function setCreateFactoryorderService(CreateFactoryorderService $createFactoryorderService): void
    {
        $this->createFactoryorderService = $createFactoryorderService;
    }

    /**
     * @return JsonModel
     */
    public function createFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // factoryorder_uuid
        $uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_factoryorder_uuid
        $uv = new Uuid();
        if (empty($uuid) || !$uv->isValid($uuid) || (!empty($uuidRepl) && !$uv->isValid($uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $filePath = $this->createFactoryorderService->createFactoryorder($uuid, $this->userService->getUserUuid(), $uuidRepl);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createFactoryorderService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
