<?php

namespace Lerp\Document\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Entity\Stream\DocOutput;
use Lerp\Document\Service\Stream\DocStreamService;

class DocStreamController extends AbstractUserController
{
    protected DocStreamService $docStreamService;

    /**
     * @param DocStreamService $docStreamService
     */
    public function setDocStreamService(DocStreamService $docStreamService): void
    {
        $this->docStreamService = $docStreamService;
    }

    /**
     * @return JsonModel
     */
    public function streamEstimateAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputEstimate($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamOfferAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputOffer($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamOrderConfirmAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputOrderConfirm($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamDeliveryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputDelivery($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamProformaAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputProforma($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamInvoiceAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputInvoice($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamPurchaseOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputPurchaseOrder($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * param[uuid] = doc_purchase_deliver_uuid
     * @return JsonModel
     */
    public function streamPurchaseDeliverAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputPurchaseDeliver($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamPurchaseRequestAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputPurchaseRequest($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function streamFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($uuid = $this->params('uuid')) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->docStreamService->outputFactoryorder($uuid, DocOutput::CONTENTDISPO_INLINE);
        return $jsonModel;
    }
}
