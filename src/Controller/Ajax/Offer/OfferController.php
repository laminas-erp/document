<?php

namespace Lerp\Document\Controller\Ajax\Offer;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\Create\Offer\CreateOfferService;

class OfferController extends AbstractUserController
{
    protected CreateOfferService $createOfferService;

    public function setCreateOfferService(CreateOfferService $createOfferService): void
    {
        $this->createOfferService = $createOfferService;
    }

    /**
     * @return JsonModel
     */
    public function createOfferAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $uuid = filter_input(INPUT_POST, 'uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // offer_uuid
        $uuidRepl = filter_input(INPUT_POST, 'uuid_replace', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]); // doc_offer_uuid
        $uuidsArr = json_decode($this->getRequest()->getPost('uuids'), true); // offer_item_uuid
        $start = filter_input(INPUT_POST, 'start', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $foot = filter_input(INPUT_POST, 'foot', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $validator = new Uuid();
        if (empty($uuid) || !$validator->isValid($uuid) || empty($uuidsArr) || !is_array($uuidsArr) || (!empty($uuidRepl) && !$validator->isValid($uuidRepl))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            if(empty($uuidsArr)){
                $jsonModel->addMessage('Es sind keine Items ausgewählt.');
            }
            return $jsonModel;
        }
        $uuids = [];
        foreach ($uuidsArr as $k => $v) {
            if ($validator->isValid($k) && is_numeric($v)) {
                $uuids[$k] = $v;
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
        }
        $filePath = $this->createOfferService->createOffer($uuid, $uuids, $start, $foot, $this->userService->getUserUuid(), $uuidRepl);
        if (empty($filePath)) {
            $jsonModel->addMessage($this->createOfferService->getMessage());
            return $jsonModel;
        }
        if (!file_exists($filePath)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
