<?php

namespace Lerp\Document\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Document\Service\PurchaseDocumentService;

class DoctItemController extends AbstractUserController
{
    protected PurchaseDocumentService $purchaseDocumentService;
    protected OrderDocumentService $orderDocumentService;

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setOrderDocumentService(OrderDocumentService $orderDocumentService): void
    {
        $this->orderDocumentService = $orderDocumentService;
    }

    /**
     * @return JsonModel
     */
    public function purchaseOrderItemsProductAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($productUuid = $this->params('product_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseDocumentService->getDocPurchaseOrderItemsForProduct($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function invoiceItemsProductAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($productUuid = $this->params('product_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->orderDocumentService->getDocInvoiceItemsForProduct($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
