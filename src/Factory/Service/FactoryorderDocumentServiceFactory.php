<?php

namespace Lerp\Document\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;

class FactoryorderDocumentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FactoryorderDocumentService();
        $service->setLogger($container->get('logger'));
        $service->setDocFactoryorderTable($container->get(DocFactoryorderTable::class));
        $service->setDocFactoryorderItemTable($container->get(DocFactoryorderItemTable::class));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setStorageLocationPdf($container->get('config')['lerp_document']['path_pdf']);
        return $service;
    }
}
