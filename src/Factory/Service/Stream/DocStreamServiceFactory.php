<?php

namespace Lerp\Document\Factory\Service\Stream;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\Stream\DocStreamService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;

class DocStreamServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new DocStreamService();
        $service->setLogger($container->get('logger'));
        $service->setPathPdf($container->get('config')['lerp_document']['path_pdf']);
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setDocOfferTable($container->get(DocOfferTable::class));
        $service->setDocEstimateTable($container->get(DocEstimateTable::class));
        $service->setDocOrderConfirmTable($container->get(DocOrderConfirmTable::class));
        $service->setDocDeliveryTable($container->get(DocDeliveryTable::class));
        $service->setDocInvoiceTable($container->get(DocInvoiceTable::class));
        $service->setDocProformaTable($container->get(DocProformaTable::class));
        $service->setDocPurchaseOrderTable($container->get(DocPurchaseOrderTable::class));
        $service->setDocPurchaseDeliverTable($container->get(DocPurchaseDeliverTable::class));
        $service->setDocPurchaseRequestTable($container->get(DocPurchaseRequestTable::class));
        $service->setDocFactoryorderTable($container->get(DocFactoryorderTable::class));
        return $service;
    }
}
