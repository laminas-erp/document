<?php

namespace Lerp\Document\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Document\Table\Order\DocDeliveryItemTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateItemTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceItemTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaItemTable;
use Lerp\Document\Table\Order\DocProformaTable;

class OrderDocumentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderDocumentService();
        $service->setLogger($container->get('logger'));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setDocEstimateTable($container->get(DocEstimateTable::class));
        $service->setDocEstimateItemTable($container->get(DocEstimateItemTable::class));
        $service->setDocOrderConfirmTable($container->get(DocOrderConfirmTable::class));
        $service->setDocOrderConfirmItemTable($container->get(DocOrderConfirmItemTable::class));
        $service->setDocDeliveryTable($container->get(DocDeliveryTable::class));
        $service->setDocDeliveryItemTable($container->get(DocDeliveryItemTable::class));
        $service->setDocInvoiceTable($container->get(DocInvoiceTable::class));
        $service->setDocInvoiceItemTable($container->get(DocInvoiceItemTable::class));
        $service->setDocProformaTable($container->get(DocProformaTable::class));
        $service->setDocProformaItemTable($container->get(DocProformaItemTable::class));
        return $service;
    }
}
