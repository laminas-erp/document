<?php

namespace Lerp\Document\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;

class PurchaseDocumentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseDocumentService();
        $service->setLogger($container->get('logger'));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setDocPurchaseOrderTable($container->get(DocPurchaseOrderTable::class));
        $service->setDocPurchaseOrderItemTable($container->get(DocPurchaseOrderItemTable::class));
        $service->setDocPurchaseDeliverTable($container->get(DocPurchaseDeliverTable::class));
        $service->setDocPurchaseDeliverItemTable($container->get(DocPurchaseDeliverItemTable::class));
        $service->setDocPurchaseRequestTable($container->get(DocPurchaseRequestTable::class));
        $service->setDocPurchaseRequestItemTable($container->get(DocPurchaseRequestItemTable::class));
        return $service;
    }
}
