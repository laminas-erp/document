<?php

namespace Lerp\Document\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Document\Table\Offer\DocOfferTable;

class OfferDocumentServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OfferDocumentService();
        $service->setLogger($container->get('logger'));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setDocOfferTable($container->get(DocOfferTable::class));
        $service->setDocOfferItemTable($container->get(DocOfferItemTable::class));
        $service->setDocOfferSendTable($container->get(DocOfferSendTable::class));
        return $service;
    }
}
