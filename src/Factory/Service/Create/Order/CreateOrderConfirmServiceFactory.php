<?php

namespace Lerp\Document\Factory\Service\Create\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CountryService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Customer\Service\CustomerService;
use Lerp\Document\Pdf\Concrete\OrderConfirmProviderInterface;
use Lerp\Document\Service\Create\Order\CreateOrderConfirmService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Unique\UniqueNumberProviderInterface;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;

class CreateOrderConfirmServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreateOrderConfirmService();
        $service->setLogger($container->get('logger'));
        $service->setLangLocaleMap($container->get('config')['bitkorn_trinket']['supported_locales_map']);
        $service->setPdfOrderConfirm($container->get(OrderConfirmProviderInterface::class));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setOrderTable($container->get(OrderTable::class));
        $service->setOrderItemTable($container->get(OrderItemTable::class));
        $service->setCustomerService($container->get(CustomerService::class));
        $service->setEquipmentUserTable($container->get(EquipmentUserTable::class));
        $service->setCountryService($container->get(CountryService::class));
        $service->setDocOrderConfirmTable($container->get(DocOrderConfirmTable::class));
        $service->setDocOrderConfirmItemTable($container->get(DocOrderConfirmItemTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        return $service;
    }
}
