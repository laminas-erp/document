<?php

namespace Lerp\Document\Factory\Service\Create\Factoryorder;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Document\Pdf\Concrete\FactoryorderProviderInterface;
use Lerp\Document\Service\Create\Factoryorder\CreateFactoryorderService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;

class CreateFactoryorderServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreateFactoryorderService();
        $service->setLogger($container->get('logger'));
        $service->setPdfFactoryorder($container->get(FactoryorderProviderInterface::class));
        $service->setFactoryorderTable($container->get(FactoryorderTable::class));
        $service->setFactoryorderWorkflowTable($container->get(FactoryorderWorkflowTable::class));
        $service->setDocFactoryorderTable($container->get(DocFactoryorderTable::class));
        $service->setDocFactoryorderItemTable($container->get(DocFactoryorderItemTable::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        return $service;
    }
}
