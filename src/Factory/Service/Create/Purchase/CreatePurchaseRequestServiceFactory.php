<?php

namespace Lerp\Document\Factory\Service\Create\Purchase;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CountryService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Document\Pdf\Concrete\PurchaseOrderProviderInterface;
use Lerp\Document\Pdf\Concrete\PurchaseRequestProviderInterface;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseRequestService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\Document\Unique\UniqueNumberProviderInterface;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Supplier\Table\SupplierTable;

class CreatePurchaseRequestServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreatePurchaseRequestService();
        $service->setLogger($container->get('logger'));
        $service->setLangLocaleMap($container->get('config')['bitkorn_trinket']['supported_locales_map']);
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setPdfPurchaseRequest($container->get(PurchaseRequestProviderInterface::class));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setPurchaseRequestTable($container->get(PurchaseRequestTable::class));
        $service->setPurchaseRequestItemTable($container->get(PurchaseRequestItemTable::class));
        $service->setSupplierTable($container->get(SupplierTable::class));
        $service->setEquipmentUserTable($container->get(EquipmentUserTable::class));
        $service->setCountryService($container->get(CountryService::class));
        $service->setDocPurchaseRequestTable($container->get(DocPurchaseRequestTable::class));
        $service->setDocPurchaseRequestItemTable($container->get(DocPurchaseRequestItemTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        return $service;
    }
}
