<?php

namespace Lerp\Document\Factory\Service\Create\Purchase;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\CountryService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Document\Pdf\Concrete\PurchaseOrderProviderInterface;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseOrderService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Unique\UniqueNumberProviderInterface;
use Lerp\Equipment\Table\User\EquipmentUserTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Supplier\Table\SupplierTable;

class CreatePurchaseOrderServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreatePurchaseOrderService();
        $service->setLogger($container->get('logger'));
        $service->setLangLocaleMap($container->get('config')['bitkorn_trinket']['supported_locales_map']);
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setPdfPurchaseOrder($container->get(PurchaseOrderProviderInterface::class));
        $service->setTranslator($container->get('DocumentTranslator'));
        $service->setPurchaseOrderTable($container->get(PurchaseOrderTable::class));
        $service->setPurchaseOrderItemTable($container->get(PurchaseOrderItemTable::class));
        $service->setSupplierTable($container->get(SupplierTable::class));
        $service->setEquipmentUserTable($container->get(EquipmentUserTable::class));
        $service->setCountryService($container->get(CountryService::class));
        $service->setDocPurchaseOrderTable($container->get(DocPurchaseOrderTable::class));
        $service->setDocPurchaseOrderItemTable($container->get(DocPurchaseOrderItemTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        return $service;
    }
}
