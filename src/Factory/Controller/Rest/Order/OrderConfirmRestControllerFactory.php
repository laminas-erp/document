<?php

namespace Lerp\Document\Factory\Controller\Rest\Order;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Controller\Rest\Order\OrderConfirmRestController;
use Lerp\Document\Service\Create\Order\CreateOrderConfirmService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OrderDocumentService;

class OrderConfirmRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderConfirmRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setDocumentService($container->get(DocumentService::class));
        $controller->setCreateOrderConfirmService($container->get(CreateOrderConfirmService::class));
        return $controller;
    }
}
