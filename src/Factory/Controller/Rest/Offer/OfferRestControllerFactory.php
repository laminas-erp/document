<?php

namespace Lerp\Document\Factory\Controller\Rest\Offer;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Controller\Rest\Offer\OfferRestController;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\OfferDocumentService;

class OfferRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OfferRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setDocumentService($container->get(DocumentService::class));
        $controller->setOfferDocumentService($container->get(OfferDocumentService::class));
        return $controller;
    }
}
