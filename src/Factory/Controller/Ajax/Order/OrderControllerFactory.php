<?php

namespace Lerp\Document\Factory\Controller\Ajax\Order;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\Create\Order\CreateDeliveryService;
use Lerp\Document\Service\Create\Order\CreateEstimateService;
use Lerp\Document\Service\Create\Order\CreateInvoiceService;
use Lerp\Document\Service\Create\Order\CreateOrderConfirmService;
use Lerp\Document\Controller\Ajax\Order\OrderController;
use Lerp\Document\Service\Create\Order\CreateProformaService;

class OrderControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OrderController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCreateEstimateService($container->get(CreateEstimateService::class));
        $controller->setCreateOrderConfirmService($container->get(CreateOrderConfirmService::class));
        $controller->setCreateDeliveryService($container->get(CreateDeliveryService::class));
        $controller->setCreateInvoiceService($container->get(CreateInvoiceService::class));
        $controller->setCreateProformaService($container->get(CreateProformaService::class));
        return $controller;
    }
}
