<?php

namespace Lerp\Document\Factory\Controller\Ajax\Purchase;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Document\Form\DocCreateForm;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseDeliverService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseOrderService;
use Lerp\Document\Service\Create\Purchase\CreatePurchaseRequestService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Controller\Ajax\Purchase\PurchaseController;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PurchaseControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setCreatePurchaseOrderService($container->get(CreatePurchaseOrderService::class));
        $controller->setCreatePurchaseDeliverService($container->get(CreatePurchaseDeliverService::class));
        $controller->setCreatePurchaseRequestService($container->get(CreatePurchaseRequestService::class));
        return $controller;
    }
}
