<?php

namespace Lerp\Document\Unique;

/**
 * A Unique consists of a number and a string. Together it is one Unique.
 *
 * Providing the following document uniques:
 * - doc_delivery_no_compl
 * - doc_estimate_no_compl
 * - doc_invoice_no_compl
 * - doc_offer_no_compl
 * - doc_order_confirm_no_compl
 * - doc_proforma_no_compl
 * - doc_purchase_deliver_no_compl
 * - doc_purchase_order_no_compl
 * - doc_purchase_request_no_compl
 *
 * ...with a number and the unique (the number plus additional characters ...company brand dependent)
 */
interface UniqueNumberProviderInterface
{
    /**
     * @param string $docType On of the DocumentService()->$docTypes
     * @return bool
     */
    public function generate(string $docType): bool;

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string;

    public function getNumber(): int;

    public function getNumberComplete(): string;

    /**
     * @param string $docType On of the DocumentService()->$docTypes
     * @return string
     */
    public function computeGetNumberComplete(string $docType): string;
}
