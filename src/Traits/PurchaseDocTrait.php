<?php

namespace Lerp\Document\Traits;

use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;

trait PurchaseDocTrait
{
    protected DocPurchaseOrderTable $docPurchaseOrderTable;
    protected DocPurchaseOrderItemTable $docPurchaseOrderItemTable;
    protected DocPurchaseDeliverTable $docPurchaseDeliverTable;
    protected DocPurchaseDeliverItemTable $docPurchaseDeliverItemTable;
    protected DocPurchaseRequestTable $docPurchaseRequestTable;
    protected DocPurchaseRequestItemTable $docPurchaseRequestItemTable;

    public function setDocPurchaseOrderTable(DocPurchaseOrderTable $docPurchaseOrderTable): void
    {
        $this->docPurchaseOrderTable = $docPurchaseOrderTable;
    }

    public function setDocPurchaseOrderItemTable(DocPurchaseOrderItemTable $docPurchaseOrderItemTable): void
    {
        $this->docPurchaseOrderItemTable = $docPurchaseOrderItemTable;
    }

    public function setDocPurchaseDeliverTable(DocPurchaseDeliverTable $docPurchaseDeliverTable): void
    {
        $this->docPurchaseDeliverTable = $docPurchaseDeliverTable;
    }

    public function setDocPurchaseDeliverItemTable(DocPurchaseDeliverItemTable $docPurchaseDeliverItemTable): void
    {
        $this->docPurchaseDeliverItemTable = $docPurchaseDeliverItemTable;
    }

    public function setDocPurchaseRequestTable(DocPurchaseRequestTable $docPurchaseRequestTable): void
    {
        $this->docPurchaseRequestTable = $docPurchaseRequestTable;
    }

    public function setDocPurchaseRequestItemTable(DocPurchaseRequestItemTable $docPurchaseRequestItemTable): void
    {
        $this->docPurchaseRequestItemTable = $docPurchaseRequestItemTable;
    }

}
