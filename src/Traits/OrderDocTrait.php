<?php

namespace Lerp\Document\Traits;

use Lerp\Document\Table\Order\DocDeliveryItemTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateItemTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceItemTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaItemTable;
use Lerp\Document\Table\Order\DocProformaTable;

trait OrderDocTrait
{
    protected DocOrderConfirmTable $docOrderConfirmTable;
    protected DocOrderConfirmItemTable $docOrderConfirmItemTable;
    protected DocEstimateTable $docEstimateTable;
    protected DocEstimateItemTable $docEstimateItemTable;
    protected DocDeliveryTable $docDeliveryTable;
    protected DocDeliveryItemTable $docDeliveryItemTable;
    protected DocInvoiceTable $docInvoiceTable;
    protected DocInvoiceItemTable $docInvoiceItemTable;
    protected DocProformaTable $docProformaTable;
    protected DocProformaItemTable $docProformaItemTable;

    public function setDocOrderConfirmTable(DocOrderConfirmTable $docOrderConfirmTable): void
    {
        $this->docOrderConfirmTable = $docOrderConfirmTable;
    }

    public function setDocOrderConfirmItemTable(DocOrderConfirmItemTable $docOrderConfirmItemTable): void
    {
        $this->docOrderConfirmItemTable = $docOrderConfirmItemTable;
    }

    public function setDocEstimateTable(DocEstimateTable $docEstimateTable): void
    {
        $this->docEstimateTable = $docEstimateTable;
    }

    public function setDocEstimateItemTable(DocEstimateItemTable $docEstimateItemTable): void
    {
        $this->docEstimateItemTable = $docEstimateItemTable;
    }

    public function setDocDeliveryTable(DocDeliveryTable $docDeliveryTable): void
    {
        $this->docDeliveryTable = $docDeliveryTable;
    }

    public function setDocDeliveryItemTable(DocDeliveryItemTable $docDeliveryItemTable): void
    {
        $this->docDeliveryItemTable = $docDeliveryItemTable;
    }

    public function setDocInvoiceTable(DocInvoiceTable $docInvoiceTable): void
    {
        $this->docInvoiceTable = $docInvoiceTable;
    }

    public function setDocInvoiceItemTable(DocInvoiceItemTable $docInvoiceItemTable): void
    {
        $this->docInvoiceItemTable = $docInvoiceItemTable;
    }

    public function setDocProformaTable(DocProformaTable $docProformaTable): void
    {
        $this->docProformaTable = $docProformaTable;
    }

    public function setDocProformaItemTable(DocProformaItemTable $docProformaItemTable): void
    {
        $this->docProformaItemTable = $docProformaItemTable;
    }

}
