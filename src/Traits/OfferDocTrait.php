<?php

namespace Lerp\Document\Traits;

use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Document\Table\Offer\DocOfferTable;

trait OfferDocTrait
{
    protected DocOfferTable $docOfferTable;
    protected DocOfferItemTable $docOfferItemTable;
    protected DocOfferSendTable $docOfferSendTable;

    public function setDocOfferTable(DocOfferTable $docOfferTable): void
    {
        $this->docOfferTable = $docOfferTable;
    }

    public function setDocOfferItemTable(DocOfferItemTable $docOfferItemTable): void
    {
        $this->docOfferItemTable = $docOfferItemTable;
    }

    public function setDocOfferSendTable(DocOfferSendTable $docOfferSendTable): void
    {
        $this->docOfferSendTable = $docOfferSendTable;
    }

}
