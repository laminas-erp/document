<?php

namespace Lerp\Document\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\MultiCheckbox;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

/**
 * Class DocCreateForm
 * @package Lerp\Document\Form
 * UNUSED, because it can not validate UUIDs in an array (field uuids).
 */
class DocCreateForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;
    protected string $tableReplace;
    protected string $tableReplacePK;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setTableReplace(string $tableReplace): void
    {
        $this->tableReplace = $tableReplace;
    }

    public function setTableReplacePK(string $tableReplacePK): void
    {
        $this->tableReplacePK = $tableReplacePK;
    }

    /**
     * @param array $tableReplaceArr ['table' => '', 'field' => '']
     * You can use function `\Lerp\Document\Service\Create\AbstractCreateDocumentService()->getTableAndPrimaryKeyFor(docType)`
     * to fetch data matching parameter `array $tableReplaceArr`.
     */
    public function setTableReplaceArray(array $tableReplaceArr): void
    {
        $this->tableReplace = $tableReplaceArr['table'];
        $this->tableReplacePK = $tableReplaceArr['field'];
    }

    public function init()
    {
        $this->add(['name' => 'uuid']);
        $this->add(['name' => 'uuid_replace']);
        $this->add(['name' => 'uuids']);
        $this->add(['name' => 'start']);
        $this->add(['name' => 'foot']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class,]
            ]
        ];

        $filter['uuid_replace'] = [
            'required' => false,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class,],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => $this->tableReplace,
                        'field' => $this->tableReplacePK
                    ]
                ]
            ]
        ];

        /**
         * @todo how to validate values in an array???
         */
        $filter['uuids'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class,]
            ]
        ];

        $filter['start'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        $filter['foot'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
