<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\Document\Traits\PurchaseDocTrait;

class PurchaseDocumentService extends AbstractService
{
    use PurchaseDocTrait;

    protected Translator $translator;

    /**
     * @param Translator $translator DocumentTranslator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return bool
     * Looks in all db.doc_* tables that belonging to an purchaseorder.
     * E.g. to see if a document exists for an item before deleting it.
     */
    public function existDocForPurchaseOrderItem(string $purchaseOrderItemUuid): bool
    {
        if (count($this->docPurchaseOrderItemTable->getDocPurchaseOrderItemsByPurchaseOrderItemUuid($purchaseOrderItemUuid)) > 0) {
            return true;
        }
        if (count($this->docPurchaseDeliverItemTable->getDocPurchaseDeliverItemsByPurchaseOrderItemUuid($purchaseOrderItemUuid)) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @return bool
     * Looks in all db.doc_* tables that belonging to an purchaserequest.
     * E.g. to see if a document exists for an item before deleting it.
     */
    public function existDocForPurchaseRequestItem(string $purchaseRequestItemUuid): bool
    {
        if (count($this->docPurchaseRequestItemTable->getDocPurchaseRequestItemsByPurchaseRequestItemUuid($purchaseRequestItemUuid)) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array
     */
    public function getDocPurchaseOrdersForPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->docPurchaseOrderTable->getDocPurchaseOrdersForPurchaseOrder($purchaseOrderUuid);
    }

    public function getDocPurchaseDeliversForPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->docPurchaseDeliverTable->getDocPurchaseDeliversForPurchaseOrder($purchaseOrderUuid);
    }

    public function getDocPurchaseOrder(string $docPurchaseOrderUuid): array
    {
        return $this->docPurchaseOrderTable->getDocPurchaseOrder($docPurchaseOrderUuid);
    }

    public function getDocPurchaseDeliver(string $docPurchaseDeliverUuid): array
    {
        return $this->docPurchaseDeliverTable->getDocPurchaseDeliver($docPurchaseDeliverUuid);
    }

    public function getDocPurchaseRequestsForPurchaseRequest(string $purchaseRequestUuid): array
    {
        return $this->docPurchaseRequestTable->getDocPurchaseRequestsForPurchaseRequest($purchaseRequestUuid);
    }

    public function getDocPurchaseRequest(string $docPurchaseRequestUuid): array
    {
        return $this->docPurchaseRequestTable->getDocPurchaseRequest($docPurchaseRequestUuid);
    }

    /**
     * @param string $productUuid
     * @return array From `view_doc_purchase_order_item`.
     */
    public function getDocPurchaseOrderItemsForProduct(string $productUuid): array
    {
        return $this->docPurchaseOrderItemTable->getDocPurchaseOrderItemsForProduct($productUuid);
    }
}
