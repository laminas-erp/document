<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;

class FactoryorderDocumentService extends AbstractService
{
    const DOC_TYPE_FACTORYORDER = 'factoryOrder';
    const FILENAME_FACTORYORDER = 'Betriebsauftrag.pdf';
    protected DocFactoryorderTable $docFactoryorderTable;
    protected DocFactoryorderItemTable $docFactoryorderItemTable;
    protected DocumentService $documentService;
    protected string $storageLocationPdf;

    public function setDocFactoryorderTable(DocFactoryorderTable $docFactoryorderTable): void
    {
        $this->docFactoryorderTable = $docFactoryorderTable;
    }

    public function setDocFactoryorderItemTable(DocFactoryorderItemTable $docFactoryorderItemTable): void
    {
        $this->docFactoryorderItemTable = $docFactoryorderItemTable;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setStorageLocationPdf(string $storageLocationPdf): void
    {
        $this->storageLocationPdf = $storageLocationPdf;
    }

    /**
     * @param string $factoryorderUuid
     * @return array Only one doc per factoryorder exist.
     */
    public function getDocFactoryorderByFactoryorderUuid(string $factoryorderUuid): array
    {
        return $this->docFactoryorderTable->getDocFactoryorderByFactoryorderUuid($factoryorderUuid);
    }

    /**
     * @param string $foWorkflowUuid
     * @return bool
     */
    public function existDocForFactoryorderWorkflow(string $foWorkflowUuid): bool
    {
        return !empty($this->docFactoryorderItemTable->getDocFactoryorderItemsByFactoryorderWorkflowUuid($foWorkflowUuid));
    }

    /**
     * @param string $docFactoryorderUuid
     * @return bool
     */
    public function deleteDocFactoryorderComplete(string $docFactoryorderUuid): bool
    {
        $df = $this->docFactoryorderTable->getDocFactoryorder($docFactoryorderUuid);
        if (empty($df)) {
            return false;
        }
        $conn = $this->beginTransaction($this->docFactoryorderTable);
        if (
            $this->docFactoryorderItemTable->deleteDocFactoryorderItemsForDocFactoryorderUuid($docFactoryorderUuid) >= 0
            && $this->docFactoryorderTable->deleteDocFactoryorder($docFactoryorderUuid) >= 0
        ) {
            /**
             * beim Ersetzen bleibt die alte Datei auch bestehen.
             */
//            $filepathPdf = $this->documentService->computeDocumentFolder(
//                    $this->storageLocationPdf,
//                    DocumentService::DOC_TYPE_FACTORYORDER,
//                    floor($df['factoryorder_time_create_unix']),
//                    $df['factoryorder_uuid']
//                ) . DIRECTORY_SEPARATOR . $df['doc_factoryorder_filename'];
//            unlink($filepathPdf);
            $conn->commit();
            return true;
        }
        $conn->rollback();
        return false;
    }
}
