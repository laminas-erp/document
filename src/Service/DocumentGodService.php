<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Table\Order\DocDeliveryItemTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateItemTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceItemTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;

class DocumentGodService extends AbstractService
{
    protected DocFactoryorderTable $docFactoryorderTable;
    protected DocFactoryorderItemTable $docFactoryorderItemTable;

    protected DocOfferTable $docOfferTable;
    protected DocOfferItemTable $docOfferItemTable;
    protected DocOfferSendTable $docOfferSendTable;

    protected DocOrderConfirmTable $docOrderConfirmTable;
    protected DocOrderConfirmItemTable $docOrderConfirmItemTable;
    protected DocEstimateTable $docEstimateTable;
    protected DocEstimateItemTable $docEstimateItemTable;
    protected DocDeliveryTable $docDeliveryTable;
    protected DocDeliveryItemTable $docDeliveryItemTable;
    protected DocInvoiceTable $docInvoiceTable;
    protected DocInvoiceItemTable $docInvoiceItemTable;

    protected DocPurchaseOrderTable $docPurchaseOrderTable;
    protected DocPurchaseOrderItemTable $docPurchaseOrderItemTable;
    protected DocPurchaseOrderSendTable $docPurchaseOrderSendTable;
    protected DocPurchaseDeliverTable $docPurchaseDeliverTable;
    protected DocPurchaseDeliverItemTable $docPurchaseDeliverItemTable;
    protected DocPurchaseDeliverSendTable $docPurchaseDeliverSendTable;

    /**
     * Delete all (in all doc_* tables) for a product.
     *
     * @param string $productUuid
     * @return bool
     */
    public function deleteProductDeep(string $productUuid): bool
    {

    }
}
