<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferSendTable;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Traits\OfferDocTrait;

class OfferDocumentService extends AbstractService
{
    use OfferDocTrait;

    protected Translator $translator;

    /**
     * @param Translator $translator DocumentTranslator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @param string $offerUuid
     * @return array From db.view_doc_offer
     */
    public function getDocOffersForOffer(string $offerUuid): array
    {
        return $this->docOfferTable->getDocOffersForOffer($offerUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_doc_offer
     */
    public function getDocOffersForOrder(string $orderUuid): array
    {
        return $this->docOfferTable->getDocOffersForOrder($orderUuid);
    }

    public function getDocOffer(string $offerUuid): array
    {
        return $this->docOfferTable->getDocOffer($offerUuid);
    }

    /**
     * @param string $offerItemUuid
     * @return bool
     * Looks in all db.doc_* tables that belonging to an offer.
     * E.g. to see if a document exists for an item before deleting it.
     */
    public function existDocForOfferItem(string $offerItemUuid): bool
    {
        if (count($this->docOfferItemTable->getDocOfferItemsByOfferItemUuid($offerItemUuid)) > 0) {
            return true;
        }
        return false;
    }

}
