<?php

namespace Lerp\Document\Service\Stream;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Document\Entity\Stream\DocOutput;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\FactoryorderDocumentService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Document\Table\Offer\DocOfferTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;

class DocStreamService extends AbstractService
{
    protected string $pathPdf;
    protected DocumentService $documentService;
    protected DocOfferTable $docOfferTable;
    protected DocEstimateTable $docEstimateTable;
    protected DocOrderConfirmTable $docOrderConfirmTable;
    protected DocDeliveryTable $docDeliveryTable;
    protected DocInvoiceTable $docInvoiceTable;
    protected DocProformaTable $docProformaTable;
    protected DocPurchaseOrderTable $docPurchaseOrderTable;
    protected DocPurchaseDeliverTable $docPurchaseDeliverTable;
    protected DocPurchaseRequestTable $docPurchaseRequestTable;
    protected DocFactoryorderTable $docFactoryorderTable;

    public function setPathPdf(string $pathPdf): void
    {
        $this->pathPdf = realpath($pathPdf);
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setDocOfferTable(DocOfferTable $docOfferTable): void
    {
        $this->docOfferTable = $docOfferTable;
    }

    public function setDocEstimateTable(DocEstimateTable $docEstimateTable): void
    {
        $this->docEstimateTable = $docEstimateTable;
    }

    public function setDocOrderConfirmTable(DocOrderConfirmTable $docOrderConfirmTable): void
    {
        $this->docOrderConfirmTable = $docOrderConfirmTable;
    }

    public function setDocDeliveryTable(DocDeliveryTable $docDeliveryTable): void
    {
        $this->docDeliveryTable = $docDeliveryTable;
    }

    public function setDocInvoiceTable(DocInvoiceTable $docInvoiceTable): void
    {
        $this->docInvoiceTable = $docInvoiceTable;
    }

    public function setDocProformaTable(DocProformaTable $docProformaTable): void
    {
        $this->docProformaTable = $docProformaTable;
    }

    public function setDocPurchaseOrderTable(DocPurchaseOrderTable $docPurchaseOrderTable): void
    {
        $this->docPurchaseOrderTable = $docPurchaseOrderTable;
    }

    public function setDocPurchaseDeliverTable(DocPurchaseDeliverTable $docPurchaseDeliverTable): void
    {
        $this->docPurchaseDeliverTable = $docPurchaseDeliverTable;
    }

    public function setDocPurchaseRequestTable(DocPurchaseRequestTable $docPurchaseRequestTable): void
    {
        $this->docPurchaseRequestTable = $docPurchaseRequestTable;
    }

    public function setDocFactoryorderTable(DocFactoryorderTable $docFactoryorderTable): void
    {
        $this->docFactoryorderTable = $docFactoryorderTable;
    }

    /**
     * @param DocOutput $docOutput
     */
    public function outputDocument(DocOutput $docOutput): void
    {
        ob_clean();
        header('Content-Type: ' . $docOutput->getContentType() . '; charset=utf-8');
        header('Content-Disposition: ' . $docOutput->getContentDispo() . '; filename="' . $docOutput->getOutFilename() . '"');
        header('Content-length: ' . filesize($docOutput->getFilePath()));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header("Pragma: no-cache");
        header("Expires: 0");
        $resource = fopen('php://output', 'w');
        $raw = file_get_contents($docOutput->getFilePath());
        fwrite($resource, $raw);
        exit();
    }

    /**
     * @param string $docOfferUuid
     * @return DocOutput|null
     */
    public function getOfferOutput(string $docOfferUuid)
    {
        $o = $this->docOfferTable->getDocOffer($docOfferUuid);
        if (empty($o)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_OFFER
            , $o['offer_time_create_unix'], $o['offer_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $o['doc_offer_filename']);
        $do->setOutFilename(DocumentService::FILENAME_OFFER_OFFER);
        return $do;
    }

    /**
     * @param string $docOfferUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputOffer(string $docOfferUuid, string $contentDisposition): void
    {
        $docOut = $this->getOfferOutput($docOfferUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docInvoiceUuid
     * @return DocOutput|null
     */
    public function getEstimateOutput(string $docInvoiceUuid)
    {
        $estimate = $this->docEstimateTable->getDocEstimate($docInvoiceUuid);
        if (empty($estimate)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_ESTIMATE
            , $estimate['order_time_create_unix'], $estimate['order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $estimate['doc_estimate_filename']);
        $do->setOutFilename(DocumentService::FILENAME_ESTIMATE);
        return $do;
    }

    /**
     * @param string $docEstimateUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputEstimate(string $docEstimateUuid, string $contentDisposition): void
    {
        $docOut = $this->getEstimateOutput($docEstimateUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docOrderConfirmUuid
     * @return DocOutput|null
     */
    public function getOrderConfirmOutput(string $docOrderConfirmUuid)
    {
        $oc = $this->docOrderConfirmTable->getDocOrderConfirm($docOrderConfirmUuid);
        if (empty($oc)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_ORDER_CONFIRM
            , $oc['order_time_create_unix'], $oc['order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $oc['doc_order_confirm_filename']);
        $do->setOutFilename(DocumentService::FILENAME_ORDER_CONFIRM);
        return $do;
    }

    /**
     * @param string $docOrderConfirmUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputOrderConfirm(string $docOrderConfirmUuid, string $contentDisposition): void
    {
        $docOut = $this->getOrderConfirmOutput($docOrderConfirmUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docDeliveryUuid
     * @return DocOutput|null
     */
    public function getDeliveryOutput(string $docDeliveryUuid)
    {
        $delivery = $this->docDeliveryTable->getDocDelivery($docDeliveryUuid);
        if (empty($delivery)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_ORDER_DELIVERY
            , $delivery['order_time_create_unix'], $delivery['order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $delivery['doc_delivery_filename']);
        $do->setOutFilename(DocumentService::FILENAME_ORDER_DELIVERY);
        return $do;
    }

    public function outputDelivery(string $docDeliveryUuid, string $contentDisposition): void
    {
        $docOut = $this->getDeliveryOutput($docDeliveryUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docInvoiceUuid
     * @return DocOutput|null
     */
    public function getInvoiceOutput(string $docInvoiceUuid)
    {
        $invoice = $this->docInvoiceTable->getDocInvoice($docInvoiceUuid);
        if (empty($invoice)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_INVOICE
            , $invoice['order_time_create_unix'], $invoice['order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $invoice['doc_invoice_filename']);
        $do->setOutFilename(DocumentService::FILENAME_INVOICE);
        return $do;
    }

    /**
     * @param string $docInvoiceUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputInvoice(string $docInvoiceUuid, string $contentDisposition): void
    {
        $docOut = $this->getInvoiceOutput($docInvoiceUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docProformaUuid
     * @return DocOutput|null
     */
    public function getProformaOutput(string $docProformaUuid)
    {
        $proforma = $this->docProformaTable->getDocProforma($docProformaUuid);
        if (empty($proforma)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_INVOICE
            , $proforma['order_time_create_unix'], $proforma['order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $proforma['doc_proforma_filename']);
        $do->setOutFilename(DocumentService::FILENAME_PROFORMA);
        return $do;
    }

    /**
     * @param string $docProformaUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputProforma(string $docProformaUuid, string $contentDisposition): void
    {
        $docOut = $this->getProformaOutput($docProformaUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docPurchaseOrderUuid
     * @return DocOutput|null
     */
    public function getPurchaseOrderOutput(string $docPurchaseOrderUuid)
    {
        $po = $this->docPurchaseOrderTable->getDocPurchaseOrder($docPurchaseOrderUuid);
        if (empty($po)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_PURCHASEORDER
            , $po['purchase_order_time_create_unix'], $po['purchase_order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $po['doc_purchase_order_filename']);
        $do->setOutFilename(DocumentService::FILENAME_PURCHASEORDER);
        return $do;
    }

    /**
     * @param string $docPurchaseOrderUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputPurchaseOrder(string $docPurchaseOrderUuid, string $contentDisposition): void
    {
        $docOut = $this->getPurchaseOrderOutput($docPurchaseOrderUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docPurchaseDeliverUuid
     * @return DocOutput|null
     */
    public function getPurchaseDeliverOutput(string $docPurchaseDeliverUuid)
    {
        $pd = $this->docPurchaseDeliverTable->getDocPurchaseDeliver($docPurchaseDeliverUuid);
        if (empty($pd)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_PURCHASEORDER_DELIVERY
            , $pd['purchase_order_time_create_unix'], $pd['purchase_order_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $pd['doc_purchase_deliver_filename']);
        $do->setOutFilename(DocumentService::FILENAME_PURCHASEORDER_DELIVERY);
        return $do;
    }

    /**
     * @param string $docPurchaseDeliverUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputPurchaseDeliver(string $docPurchaseDeliverUuid, string $contentDisposition): void
    {
        $docOut = $this->getPurchaseDeliverOutput($docPurchaseDeliverUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docPurchaseRequestUuid
     * @return DocOutput|null
     */
    public function getPurchaseRequestOutput(string $docPurchaseRequestUuid)
    {
        $pr = $this->docPurchaseRequestTable->getDocPurchaseRequest($docPurchaseRequestUuid);
        if (empty($pr)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            DocumentService::DOC_TYPE_PURCHASEORDER_REQUEST
            , $pr['purchase_request_time_create_unix'], $pr['purchase_request_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $pr['doc_purchase_request_filename']);
        $do->setOutFilename(DocumentService::FILENAME_PURCHASEORDER_REQUEST);
        return $do;
    }

    /**
     * @param string $docPurchaseRequestUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputPurchaseRequest(string $docPurchaseRequestUuid, string $contentDisposition): void
    {
        $docOut = $this->getPurchaseRequestOutput($docPurchaseRequestUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }

    /**
     * @param string $docFactoryorderUuid
     * @return DocOutput|null
     */
    public function getFactoryorderOutput(string $docFactoryorderUuid)
    {
        $fo = $this->docFactoryorderTable->getDocFactoryorder($docFactoryorderUuid);
        if (empty($fo)) {
            return null;
        }
        $folder = $this->documentService->getDocumentFolder(
            FactoryorderDocumentService::DOC_TYPE_FACTORYORDER
            , $fo['factoryorder_time_create_unix'], $fo['factoryorder_uuid']);
        $do = new DocOutput();
        $do->setFilePath($this->pathPdf . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $fo['doc_factoryorder_filename']);
        $do->setOutFilename(FactoryorderDocumentService::FILENAME_FACTORYORDER);
        return $do;
    }

    /**
     * @param string $docFactoryorderUuid
     * @param string $contentDisposition DocOutput::CONTENTDISPO_ATTACHMENT or DocOutput::CONTENTDISPO_INLINE
     */
    public function outputFactoryorder(string $docFactoryorderUuid, string $contentDisposition): void
    {
        $docOut = $this->getFactoryorderOutput($docFactoryorderUuid);
        $docOut->setContentDispo($contentDisposition);
        $this->outputDocument($docOut);
    }
}
