<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Traits\OrderDocTrait;

class OrderDocumentService extends AbstractService
{
    use OrderDocTrait;

    protected Translator $translator;

    /**
     * @param Translator $translator DocumentTranslator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getDocEstimatesForOrder(string $orderUuid): array
    {
        return $this->docEstimateTable->getDocEstimatesForOrder($orderUuid);
    }

    public function getDocEstimate(string $docEstimateUuid): array
    {
        return $this->docEstimateTable->getDocEstimate($docEstimateUuid);
    }

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getDocOrderConfirmsForOrder(string $orderUuid): array
    {
        return $this->docOrderConfirmTable->getDocOrderConfirmsForOrder($orderUuid);
    }

    public function getDocOrderConfirm(string $docOrderConfirmUuid): array
    {
        return $this->docOrderConfirmTable->getDocOrderConfirm($docOrderConfirmUuid);
    }

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getDocDeliverysForOrder(string $orderUuid): array
    {
        return $this->docDeliveryTable->getDocDeliverysForOrder($orderUuid);
    }

    public function getDocDelivery(string $docDeliveryUuid): array
    {
        return $this->docDeliveryTable->getDocDelivery($docDeliveryUuid);
    }

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getDocInvoicesForOrder(string $orderUuid): array
    {
        return $this->docInvoiceTable->getDocInvoicesForOrder($orderUuid);
    }

    public function getDocInvoice(string $docInvoiceUuid): array
    {
        return $this->docInvoiceTable->getDocInvoice($docInvoiceUuid);
    }

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getDocProformasForOrder(string $orderUuid): array
    {
        return $this->docProformaTable->getDocProformasForOrder($orderUuid);
    }

    public function getDocProforma(string $docProformaUuid): array
    {
        return $this->docProformaTable->getDocProforma($docProformaUuid);
    }

    /**
     * @param string $orderItemUuid
     * @return bool
     * Looks in all db.doc_* tables that belonging to an order.
     * E.g. to see if a document exists for an item before deleting it.
     */
    public function existDocForOrderItem(string $orderItemUuid): bool
    {
        if (count($this->docOrderConfirmItemTable->getDocOrderConfirmItemsByOrderItemUuid($orderItemUuid)) > 0) {
            return true;
        }
        if (count($this->docEstimateItemTable->getDocEstimateItemsByOrderItemUuid($orderItemUuid)) > 0) {
            return true;
        }
        if (count($this->docDeliveryItemTable->getDocDeliveryItemsByOrderItemUuid($orderItemUuid)) > 0) {
            return true;
        }
        if (count($this->docInvoiceItemTable->getDocInvoiceItemsByOrderItemUuid($orderItemUuid)) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $productUuid
     * @return array From `view_doc_invoice_item`.
     */
    public function getDocInvoiceItemsForProduct(string $productUuid): array
    {
        return $this->docInvoiceItemTable->getDocInvoiceItemsForProduct($productUuid);
    }
}
