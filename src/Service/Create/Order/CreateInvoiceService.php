<?php

namespace Lerp\Document\Service\Create\Order;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\InvoiceProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;

class CreateInvoiceService extends AbstractCreateOrderService
{
    protected DocumentProviderInterface|InvoiceProviderInterface $pdfInvoice;
    protected array $invoiceTypes;

    public function setPdfInvoice(DocumentProviderInterface|InvoiceProviderInterface $pdfInvoice): void
    {
        $this->pdfInvoice = $pdfInvoice;
    }

    public function setInvoiceTypes(array $invoiceTypes): void
    {
        $this->invoiceTypes = $invoiceTypes;
    }

    public function createInvoice(string $orderUuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl, string $invoiceType, bool $ignorePaidDelivered): string
    {
        if (!in_array($invoiceType, $this->invoiceTypes)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' - Invalid doc_invoice type');
        }
        if (empty($order = $this->orderTable->getOrder($orderUuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($customer = $this->customerService->getCustomer($order['customer_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($order['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_INVOICE))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$order['order_lang_iso']]);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfInvoice->setDocInvoiceNoCompl($newDocNoCompl);
        } else if (!empty($docInvoice = $this->docInvoiceTable->getDocInvoiceMini($uuidRepl))) {
            $this->pdfInvoice->setDocInvoiceNoCompl($docInvoice['doc_invoice_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->pdfInvoice->setInvoiceType($invoiceType);
        if (empty($entities = $this->computeOrderItemEntities($itemUuids, DocumentService::DOC_TYPE_INVOICE, $uuidRepl, $ignorePaidDelivered))) {
            return '';
        }
        $this->pdfInvoice->setOrderItemEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($customer['customer_name'])
                , html_entity_decode($customer['customer_street'])
                , html_entity_decode($customer['customer_zip'] . ' ' . $customer['customer_city'])
                , html_entity_decode($this->countryService->getCountryLabel($customer['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfInvoice->setBaseDataEntity($baseData);
        $this->pdfInvoice->makeDocument();
        $priceSum = $this->pdfInvoice->getPriceSum();
        $taxSum = $this->pdfInvoice->getTaxSum();
        $path = $this->pdfInvoice->writeDocument();
        if (!file_exists($path)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docInvoiceTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docInvoiceTable->updateDocInvoice($uuidRepl, basename($path), $userUuid, $priceSum, $taxSum);
            if ($this->docInvoiceItemTable->deleteDocInvoiceItemsForDocInvoiceUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docInvoiceTable->insertDocInvoice(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $orderUuid, $userUuid, $priceSum, $taxSum, $invoiceType);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docInvoiceItemTable->insertDocInvoiceItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }
}
