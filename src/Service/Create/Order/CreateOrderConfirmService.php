<?php

namespace Lerp\Document\Service\Create\Order;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\OrderConfirmProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;

class CreateOrderConfirmService extends AbstractCreateOrderService
{
    protected DocumentProviderInterface|OrderConfirmProviderInterface $pdfOrderConfirm;

    public function setPdfOrderConfirm(DocumentProviderInterface|OrderConfirmProviderInterface $pdfOrderConfirm): void
    {
        $this->pdfOrderConfirm = $pdfOrderConfirm;
    }

    /**
     * @param string $orderUuid order_uuid
     * @param array $itemUuids Assoc: key=uuid; value=quantity
     * @param string $start
     * @param string $foot
     * @param string $userUuid
     * @param string $uuidRepl
     * @return string The path of the new created document.
     */
    public function createOrderConfirm(string $orderUuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl, bool $ignorePaidDelivered): string
    {
        if (empty($order = $this->orderTable->getOrder($orderUuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($customer = $this->customerService->getCustomer($order['customer_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($order['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_ORDER_CONFIRM))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$order['order_lang_iso']]);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfOrderConfirm->setDocOrderConfirmNoCompl($newDocNoCompl);
        } else if (!empty($docOrderConfirm = $this->docOrderConfirmTable->getDocOrderConfirmMini($uuidRepl))) {
            $this->pdfOrderConfirm->setDocOrderConfirmNoCompl($docOrderConfirm['doc_order_confirm_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($entities = $this->computeOrderItemEntities($itemUuids, DocumentService::DOC_TYPE_ORDER_CONFIRM, $uuidRepl, $ignorePaidDelivered))) {
            return '';
        }
        $this->pdfOrderConfirm->setOrderItemEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($customer['customer_name'])
                , html_entity_decode($customer['customer_street'])
                , html_entity_decode($customer['customer_zip'] . ' ' . $customer['customer_city'])
                , html_entity_decode($this->countryService->getCountryLabel($customer['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfOrderConfirm->setBaseDataEntity($baseData);
        $this->pdfOrderConfirm->makeDocument();
        $priceSum = $this->pdfOrderConfirm->getPriceSum();
        $taxSum = $this->pdfOrderConfirm->getTaxSum();
        $path = $this->pdfOrderConfirm->writeDocument();
        if (!file_exists($path)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docOrderConfirmTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docOrderConfirmTable->updateDocOrderConfirm($uuidRepl, basename($path), $userUuid, $priceSum, $taxSum);
            if ($this->docOrderConfirmItemTable->deleteDocOrderConfirmItemsForDocOrderConfirmUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docOrderConfirmTable->insertDocOrderConfirm(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $orderUuid, $userUuid, $priceSum, $taxSum);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docOrderConfirmItemTable->insertDocOrderConfirmItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }

    public function getDocOrderConfirmsForOrder(string $orderUuid): array
    {
        return $this->docOrderConfirmTable->getDocOrderConfirmsForOrder($orderUuid);
    }
}
