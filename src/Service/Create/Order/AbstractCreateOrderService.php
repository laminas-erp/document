<?php

namespace Lerp\Document\Service\Create\Order;

use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Document\Service\Create\AbstractCreateService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Order\DocDeliveryItemTable;
use Lerp\Document\Table\Order\DocDeliveryTable;
use Lerp\Document\Table\Order\DocEstimateItemTable;
use Lerp\Document\Table\Order\DocEstimateTable;
use Lerp\Document\Table\Order\DocInvoiceItemTable;
use Lerp\Document\Table\Order\DocInvoiceTable;
use Lerp\Document\Table\Order\DocOrderConfirmItemTable;
use Lerp\Document\Table\Order\DocOrderConfirmTable;
use Lerp\Document\Table\Order\DocProformaItemTable;
use Lerp\Document\Table\Order\DocProformaTable;
use Lerp\Order\Entity\Order\OrderItemEntity;
use Lerp\Order\Table\Order\OrderItemTable;
use Lerp\Order\Table\Order\OrderTable;

abstract class AbstractCreateOrderService extends AbstractCreateService
{
    protected OrderTable $orderTable;
    protected OrderItemTable $orderItemTable;
    protected DocDeliveryTable $docDeliveryTable;
    protected DocDeliveryItemTable $docDeliveryItemTable;
    protected DocEstimateTable $docEstimateTable;
    protected DocEstimateItemTable $docEstimateItemTable;
    protected DocInvoiceTable $docInvoiceTable;
    protected DocInvoiceItemTable $docInvoiceItemTable;
    protected DocOrderConfirmTable $docOrderConfirmTable;
    protected DocOrderConfirmItemTable $docOrderConfirmItemTable;
    protected DocProformaTable $docProformaTable;
    protected DocProformaItemTable $docProformaItemTable;

    public function setOrderTable(OrderTable $orderTable): void
    {
        $this->orderTable = $orderTable;
    }

    public function setOrderItemTable(OrderItemTable $orderItemTable): void
    {
        $this->orderItemTable = $orderItemTable;
    }

    public function setDocDeliveryTable(DocDeliveryTable $docDeliveryTable): void
    {
        $this->docDeliveryTable = $docDeliveryTable;
    }

    public function setDocDeliveryItemTable(DocDeliveryItemTable $docDeliveryItemTable): void
    {
        $this->docDeliveryItemTable = $docDeliveryItemTable;
    }

    public function setDocEstimateTable(DocEstimateTable $docEstimateTable): void
    {
        $this->docEstimateTable = $docEstimateTable;
    }

    public function setDocEstimateItemTable(DocEstimateItemTable $docEstimateItemTable): void
    {
        $this->docEstimateItemTable = $docEstimateItemTable;
    }

    public function setDocInvoiceTable(DocInvoiceTable $docInvoiceTable): void
    {
        $this->docInvoiceTable = $docInvoiceTable;
    }

    public function setDocInvoiceItemTable(DocInvoiceItemTable $docInvoiceItemTable): void
    {
        $this->docInvoiceItemTable = $docInvoiceItemTable;
    }

    public function setDocOrderConfirmTable(DocOrderConfirmTable $docOrderConfirmTable): void
    {
        $this->docOrderConfirmTable = $docOrderConfirmTable;
    }

    public function setDocOrderConfirmItemTable(DocOrderConfirmItemTable $docOrderConfirmItemTable): void
    {
        $this->docOrderConfirmItemTable = $docOrderConfirmItemTable;
    }

    public function setDocProformaTable(DocProformaTable $docProformaTable): void
    {
        $this->docProformaTable = $docProformaTable;
    }

    public function setDocProformaItemTable(DocProformaItemTable $docProformaItemTable): void
    {
        $this->docProformaItemTable = $docProformaItemTable;
    }

    /**
     * Compare $itemUuids item_quantity with orderItems item_quantity.
     * Checks, if itemQuantity is not bigger than the base (from order_item) quantity.
     * @param array $itemUuids Action items assoc: [item_uuid => item_quantity]
     * @return OrderItemEntity[] If the array empty then there will be a $this->message.
     */
    protected function computeOrderItemEntities(array $itemUuids, string $docType, string $docUuidRepl = '', bool $ignorePaidDelivered = false): array
    {
        if (
            !in_array($docType, $this->documentService->getDocTypes())
            || empty($itemUuids) || !is_array($itemUuids)
            || empty($items = $this->orderItemTable->getOrderItemsWithUuids(array_keys($itemUuids)))
            || count($itemUuids) != count($items)
        ) {
            $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
            return [];
        }
        $entities = [];
        foreach ($items as $item) {
            $entity = new OrderItemEntity();
            $entity->exchangeArrayFromDatabase($item);
            if ($itemUuids[$entity->getUuid()] > $entity->getOrderItemQuantity()) {
                $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
                return [];
            }
            $entity->backupStorageToOutsite();
            $entity->setOrderItemQuantity($itemUuids[$entity->getUuid()]);
            $entity->setOrderItemPriceTotal($itemUuids[$entity->getUuid()] * $entity->getOrderItemPrice());
            if (
                !$ignorePaidDelivered && (empty($docUuidRepl) && !$this->checkOrderItemQuantityForDocumentType($entity, $docType))
                || (!$ignorePaidDelivered && !empty($docUuidRepl) && !$this->checkOrderItemQuantityForDocumentTypeReplace($entity, $docType, $docUuidRepl))
            ) {
                // $this->message is set in the called function
                return [];
            }
            $entities[] = $entity;
        }
        return $entities;
    }

    /**
     * Check the quantity of an order_item against what is already done.
     * @param OrderItemEntity $entity Entity with origin storage backup in storageOutsite. E.g. from $this->computeOrderItemEntities(array $itemUuids).
     * @param string $docType
     * @return bool If FALSE then there will be a $this->message.
     */
    protected function checkOrderItemQuantityForDocumentType(OrderItemEntity $entity, string $docType): bool
    {
        if (!in_array($docType, $this->documentService->getDocTypes())) {
            $this->message = 'Falscher Dokument Typ: ' . $docType;
            return false;
        }
        $oiQnttyOrigin = floatval($entity->getStorageOutsiteValue('order_item_quantity'));
        $oiQnttyTarget = $entity->getOrderItemQuantity();
        switch ($docType) {
            case DocumentService::DOC_TYPE_ORDER_DELIVERY:
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumDeliveryQntty())) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ESTIMATE:
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumEstimateQntty())) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_INVOICE:
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumInvoiceQntty())) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ORDER_CONFIRM:
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumOrderConfirmQntty())) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_PROFORMA:
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumProformaQntty())) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
        }
        return true;
    }

    protected array $docDeliveryItems;
    protected array $docEstimateItems;
    protected array $docInvoiceItems;
    protected array $docOrderConfirmItems;
    protected array $docProformaItems;

    /**
     * Quantity unit from current orderItem can be different to the docItem quantity unit!
     * No check for amount_bigger_than_origin! 'amount_bigger_than_origin' already checked in $this->computeOrderItemEntities()
     * @param OrderItemEntity $entity Entity with origin storage backup in storageOutsite. E.g. from $this->computeOrderItemEntities(array $itemUuids).
     * @param string $docType
     * @param string $docUuidRepl
     * @return bool
     */
    protected function checkOrderItemQuantityForDocumentTypeReplace(OrderItemEntity $entity, string $docType, string $docUuidRepl): bool
    {
        if (!in_array($docType, $this->documentService->getDocTypes())) {
            $this->message = 'Falscher Dokument Typ: ' . $docType;
            return false;
        }
        $oiQnttyOrigin = floatval($entity->getStorageOutsiteValue('order_item_quantity'));
        $oiQnttyTarget = $entity->getOrderItemQuantity();
        switch ($docType) {
            case DocumentService::DOC_TYPE_ORDER_DELIVERY:
                if (!isset($this->docDeliveryItems) && empty($this->docDeliveryItems = $this->docDeliveryItemTable->getDocDeliveryItemsByDocDeliveryUuid($docUuidRepl))) {
                    $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
                    return false;
                }
                if (!isset($this->docDeliveryItems[$entity->getOrderItemUuid()])) {
                    // new order_item - amount_bigger_than_origin already checked in $this->computeOrderItemEntities()
                    return true;
                }
                $docItem = $this->docDeliveryItems[$entity->getOrderItemUuid()];
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumDeliveryQntty() + $docItem['order_item_quantity'])) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ESTIMATE:
                if (!isset($this->docEstimateItems) && empty($this->docEstimateItems = $this->docEstimateItemTable->getDocEstimateItemsByDocEstimateUuid($docUuidRepl))) {
                    $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
                    return false;
                }
                if (!isset($this->docEstimateItems[$entity->getOrderItemUuid()])) {
                    // new order_item - amount_bigger_than_origin already checked in $this->computeOrderItemEntities()
                    return true;
                }
                $docItem = $this->docEstimateItems[$entity->getOrderItemUuid()];
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumEstimateQntty() + $docItem['order_item_quantity'])) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_INVOICE:
                if (!isset($this->docInvoiceItems) && empty($this->docInvoiceItems = $this->docInvoiceItemTable->getDocInvoiceItemsByDocInvoiceUuid($docUuidRepl))) {
                    $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
                    return false;
                }
                if (!isset($this->docInvoiceItems[$entity->getOrderItemUuid()])) {
                    // new order_item - amount_bigger_than_origin already checked in $this->computeOrderItemEntities()
                    return true;
                }
                $docItem = $this->docInvoiceItems[$entity->getOrderItemUuid()];
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumInvoiceQntty() + $docItem['order_item_quantity'])) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_ORDER_CONFIRM:
                if (!isset($this->docOrderConfirmItems) && empty($this->docOrderConfirmItems = $this->docOrderConfirmItemTable->getDocOrderConfirmItemsByDocOrderConfirmUuid($docUuidRepl))) {
                    $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
                    return false;
                }
                if (!isset($this->docOrderConfirmItems[$entity->getOrderItemUuid()])) {
                    // new order_item - amount_bigger_than_origin already checked in $this->computeOrderItemEntities()
                    return true;
                }
                $docItem = $this->docOrderConfirmItems[$entity->getOrderItemUuid()];
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumOrderConfirmQntty() + $docItem['order_item_quantity'])) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
            case DocumentService::DOC_TYPE_PROFORMA:
                if (!isset($this->docProformaItems) && empty($this->docProformaItems = $this->docProformaItemTable->getDocProformaItemsByDocProformaUuid($docUuidRepl))) {
                    $this->message = 'Fatal Error in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__;
                    return false;
                }
                if (!isset($this->docProformaItems[$entity->getOrderItemUuid()])) {
                    // new order_item - amount_bigger_than_origin already checked in $this->computeOrderItemEntities()
                    return true;
                }
                $docItem = $this->docProformaItems[$entity->getOrderItemUuid()];
                if ($oiQnttyTarget > ($oiQnttyOrigin - $entity->getSumProformaQntty() + $docItem['order_item_quantity'])) {
                    $this->message = 'Pos. ' . $entity->getOrderItemId() . ' ' . $this->translator->translate('amount_bigger_than_rest', 'lerp_doc');
                    return false;
                }
                break;
        }
        return true;
    }
}
