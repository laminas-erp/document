<?php

namespace Lerp\Document\Service\Create\Purchase;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\PurchaseOrderProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Supplier\Table\SupplierTable;

class CreatePurchaseOrderService extends AbstractCreatePurchaseService
{
    protected DocumentProviderInterface|PurchaseOrderProviderInterface $pdfPurchaseOrder;
    protected DocPurchaseOrderTable $docPurchaseOrderTable;
    protected DocPurchaseOrderItemTable $docPurchaseOrderItemTable;
    protected SupplierTable $supplierTable;

    public function setPdfPurchaseOrder(DocumentProviderInterface|PurchaseOrderProviderInterface $pdfPurchaseOrder): void
    {
        $this->pdfPurchaseOrder = $pdfPurchaseOrder;
    }

    public function setDocPurchaseOrderTable(DocPurchaseOrderTable $docPurchaseOrderTable): void
    {
        $this->docPurchaseOrderTable = $docPurchaseOrderTable;
    }

    public function setDocPurchaseOrderItemTable(DocPurchaseOrderItemTable $docPurchaseOrderItemTable): void
    {
        $this->docPurchaseOrderItemTable = $docPurchaseOrderItemTable;
    }

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    /**
     * @param string $uuid purchase_order_uuid
     * @param array $itemUuids Assoc: key=uuid; value=quantity
     * @param string $start
     * @param string $foot
     * @param string $userUuid Session user
     * @param string $uuidRepl
     * @return string The doc path.
     */
    public function createPurchaseOrder(string $uuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl): string
    {
        if (empty($po = $this->purchaseOrderTable->getPurchaseOrder($uuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($items = $this->purchaseOrderItemTable->getPurchaseOrderItemsWithUuids(array_keys($itemUuids)))
            || empty($supplier = $this->supplierTable->getSupplierBase($po['supplier_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($po['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_PURCHASEORDER))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$supplier['supplier_lang_iso']]);
        $this->pdfPurchaseOrder->setPurchaseOrderNoCompl($po['purchase_order_no_compl']);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfPurchaseOrder->setDocPurchaseOrderNoCompl($newDocNoCompl);
        } else if (!empty($docPurchaseOrder = $this->docPurchaseOrderTable->getDocPurchaseOrderMini($uuidRepl))) {
            $this->pdfPurchaseOrder->setDocPurchaseOrderNoCompl($docPurchaseOrder['doc_purchase_order_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($entities = $this->computePurchaseOrderItemEntities($items, $itemUuids))) {
            $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
            return '';
        }
        $this->pdfPurchaseOrder->setPurchaseOrderItemEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($supplier['supplier_name'])
                , html_entity_decode($supplier['supplier_street'])
                , html_entity_decode($supplier['supplier_zip'] . ' ' . $supplier['supplier_city'])
                , html_entity_decode($this->countryService->getCountryLabel($supplier['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfPurchaseOrder->setBaseDataEntity($baseData);
        $this->pdfPurchaseOrder->makeDocument();
        $priceSum = $this->pdfPurchaseOrder->getPriceSum();
        $taxSum = $this->pdfPurchaseOrder->getTaxSum();
        $path = $this->pdfPurchaseOrder->writeDocument();
        if (!file_exists($path)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create PDF purchaseOrder for ' . $uuid);
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docPurchaseOrderTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docPurchaseOrderTable->updateDocPurchaseOrder($uuidRepl, basename($path), $userUuid, $priceSum, $taxSum);
            if ($this->docPurchaseOrderItemTable->deleteDocPurchaseOrderItemsForDocPurchaseOrderUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docPurchaseOrderTable->insertDocPurchaseOrder(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $uuid, $userUuid, $priceSum, $taxSum);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docPurchaseOrderItemTable->insertDocPurchaseOrderItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }
}
