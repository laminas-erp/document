<?php

namespace Lerp\Document\Service\Create\Purchase;

use Lerp\Document\Service\Create\AbstractCreateService;
use Lerp\Purchase\Entity\PurchaseOrderItemAttachEntity;
use Lerp\Purchase\Entity\PurchaseOrderItemEntity;
use Lerp\Purchase\Entity\PurchaseRequestItemEntity;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;

abstract class AbstractCreatePurchaseService extends AbstractCreateService
{
    protected PurchaseOrderTable $purchaseOrderTable;
    protected PurchaseOrderItemTable $purchaseOrderItemTable;
    protected PurchaseRequestTable $purchaseRequestTable;
    protected PurchaseRequestItemTable $purchaseRequestItemTable;

    public function setPurchaseOrderTable(PurchaseOrderTable $purchaseOrderTable): void
    {
        $this->purchaseOrderTable = $purchaseOrderTable;
    }

    public function setPurchaseOrderItemTable(PurchaseOrderItemTable $purchaseOrderItemTable): void
    {
        $this->purchaseOrderItemTable = $purchaseOrderItemTable;
    }

    public function setPurchaseRequestTable(PurchaseRequestTable $purchaseRequestTable): void
    {
        $this->purchaseRequestTable = $purchaseRequestTable;
    }

    public function setPurchaseRequestItemTable(PurchaseRequestItemTable $purchaseRequestItemTable): void
    {
        $this->purchaseRequestItemTable = $purchaseRequestItemTable;
    }

    /**
     * @param array $items
     * @param array $itemUuids
     * @return PurchaseOrderItemEntity[]
     */
    protected function computePurchaseOrderItemEntities(array $items, array $itemUuids): array
    {
        $entities = [];
        foreach ($items as $item) {
            $e = new PurchaseOrderItemEntity();
            $e->exchangeArrayFromDatabase($item);
            if ($itemUuids[$e->getUuid()] > $e->getPurchaseOrderItemQuantity()) {
                $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
                return [];
            }
            $e->setPurchaseOrderItemQuantity($itemUuids[$e->getUuid()]);
            $e->setPurchaseOrderItemPriceTotal($itemUuids[$e->getUuid()] * $e->getPurchaseOrderItemPrice());
            $entities[] = $e;
        }
        return $entities;
    }

    /**
     * @param array $items
     * @param array $itemUuids
     * @return PurchaseRequestItemEntity[]
     */
    protected function computePurchaseRequestItemEntities(array $items, array $itemUuids): array
    {
        $entities = [];
        foreach ($items as $item) {
            $e = new PurchaseRequestItemEntity();
            $e->exchangeArrayFromDatabase($item);
            if ($itemUuids[$e->getUuid()] > $e->getPurchaseRequestItemQuantity()) {
                $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
                return [];
            }
            $e->setPurchaseRequestItemQuantity($itemUuids[$e->getUuid()]);
            $entities[] = $e;
        }
        return $entities;
    }

    protected function computePurchaseOrderItemAttachEntities(array $items, array $itemUuids): array
    {
        $entities = [];
        foreach ($items as $item) {
            $e = new PurchaseOrderItemAttachEntity();
            $e->exchangeArrayFromDatabase($item);
            if ($itemUuids[$e->getUuid()] > $e->getPurchaseOrderItemAttachQuantity()) {
                $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
                return [];
            }
            $e->setPurchaseOrderItemAttachQuantity($itemUuids[$e->getUuid()]);
            $entities[] = $e;
        }
        return $entities;
    }
}
