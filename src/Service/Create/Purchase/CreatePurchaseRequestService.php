<?php

namespace Lerp\Document\Service\Create\Purchase;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\PurchaseRequestProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseRequestItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\Supplier\Table\SupplierTable;

class CreatePurchaseRequestService extends AbstractCreatePurchaseService
{
    protected DocumentProviderInterface|PurchaseRequestProviderInterface $pdfPurchaseRequest;
    protected DocPurchaseRequestTable $docPurchaseRequestTable;
    protected DocPurchaseRequestItemTable $docPurchaseRequestItemTable;
    protected SupplierTable $supplierTable;

    public function setPdfPurchaseRequest(DocumentProviderInterface|PurchaseRequestProviderInterface $pdfPurchaseRequest): void
    {
        $this->pdfPurchaseRequest = $pdfPurchaseRequest;
    }

    public function setDocPurchaseRequestTable(DocPurchaseRequestTable $docPurchaseRequestTable): void
    {
        $this->docPurchaseRequestTable = $docPurchaseRequestTable;
    }

    public function setDocPurchaseRequestItemTable(DocPurchaseRequestItemTable $docPurchaseRequestItemTable): void
    {
        $this->docPurchaseRequestItemTable = $docPurchaseRequestItemTable;
    }

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    /**
     * @param string $uuid purchase_order_uuid
     * @param array $itemUuids Assoc: key=uuid; value=quantity
     * @param string $start
     * @param string $foot
     * @param string $userUuid Session user
     * @param string $uuidRepl
     * @return string The doc path.
     */
    public function createPurchaseRequest(string $uuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl): string
    {
        if (empty($pr = $this->purchaseRequestTable->getPurchaseRequest($uuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($items = $this->purchaseRequestItemTable->getPurchaseRequestItemsWithUuids(array_keys($itemUuids)))
            || empty($supplier = $this->supplierTable->getSupplierBase($pr['supplier_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($pr['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_PURCHASEORDER_REQUEST))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$supplier['supplier_lang_iso']]);
        $this->pdfPurchaseRequest->setPurchaseRequestNoCompl($pr['purchase_request_no_compl']);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfPurchaseRequest->setDocPurchaseRequestNoCompl($newDocNoCompl);
        } else if (!empty($docPurchaseRequest = $this->docPurchaseRequestTable->getDocPurchaseRequestMini($uuidRepl))) {
            $this->pdfPurchaseRequest->setDocPurchaseRequestNoCompl($docPurchaseRequest['doc_purchase_request_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($entities = $this->computePurchaseRequestItemEntities($items, $itemUuids))) {
            $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
            return '';
        }
        $this->pdfPurchaseRequest->setPurchaseRequestItemEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($supplier['supplier_name'])
                , html_entity_decode($supplier['supplier_street'])
                , html_entity_decode($supplier['supplier_zip'] . ' ' . $supplier['supplier_city'])
                , html_entity_decode($this->countryService->getCountryLabel($supplier['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfPurchaseRequest->setBaseDataEntity($baseData);
        $this->pdfPurchaseRequest->makeDocument();
        $path = $this->pdfPurchaseRequest->writeDocument();
        if (!file_exists($path)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create PDF purchaseRequest for ' . $uuid);
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docPurchaseRequestTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docPurchaseRequestTable->updateDocPurchaseRequest($uuidRepl, basename($path), $userUuid);
            if ($this->docPurchaseRequestItemTable->deleteDocPurchaseRequestItemsForDocPurchaseRequestUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docPurchaseRequestTable->insertDocPurchaseRequest(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $uuid, $userUuid);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docPurchaseRequestItemTable->insertDocPurchaseRequestItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }
}
