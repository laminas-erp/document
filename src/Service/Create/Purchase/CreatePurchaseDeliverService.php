<?php

namespace Lerp\Document\Service\Create\Purchase;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\PurchaseDeliverProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemAttachTable;
use Lerp\Supplier\Table\SupplierTable;

class CreatePurchaseDeliverService extends AbstractCreatePurchaseService
{
    protected DocumentProviderInterface|PurchaseDeliverProviderInterface $pdfPurchaseDeliver;
    protected DocPurchaseDeliverTable $docPurchaseDeliverTable;
    protected DocPurchaseDeliverItemTable $docPurchaseDeliverItemTable;
    protected SupplierTable $supplierTable;
    protected PurchaseOrderItemAttachTable $purchaseOrderItemAttachTable;

    public function setPdfPurchaseDeliver(DocumentProviderInterface|PurchaseDeliverProviderInterface $pdfPurchaseDeliver): void
    {
        $this->pdfPurchaseDeliver = $pdfPurchaseDeliver;
    }

    public function setDocPurchaseDeliverTable(DocPurchaseDeliverTable $docPurchaseDeliverTable): void
    {
        $this->docPurchaseDeliverTable = $docPurchaseDeliverTable;
    }

    public function setDocPurchaseDeliverItemTable(DocPurchaseDeliverItemTable $docPurchaseDeliverItemTable): void
    {
        $this->docPurchaseDeliverItemTable = $docPurchaseDeliverItemTable;
    }

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    public function setPurchaseOrderItemAttachTable(PurchaseOrderItemAttachTable $purchaseOrderItemAttachTable): void
    {
        $this->purchaseOrderItemAttachTable = $purchaseOrderItemAttachTable;
    }

    /**
     * @param string $uuid purchase_order_uuid
     * @param array $itemUuids Assoc: key=uuid; value=quantity
     * @param string $start
     * @param string $foot
     * @param string $userUuid Session user
     * @param string $uuidRepl
     * @return string The doc path.
     */
    public function createPurchaseDeliver(string $uuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl): string
    {
        if (empty($po = $this->purchaseOrderTable->getPurchaseOrder($uuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($items = $this->purchaseOrderItemAttachTable->getPurchaseOrderItemAttachsWithUuids(array_keys($itemUuids)))
            || empty($supplier = $this->supplierTable->getSupplierBase($po['supplier_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($po['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_PURCHASEORDER_DELIVERY))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$supplier['supplier_lang_iso']]);
        $this->pdfPurchaseDeliver->setPurchaseOrderNoCompl($po['purchase_order_no_compl']);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfPurchaseDeliver->setDocPurchaseDeliverNoCompl($newDocNoCompl);
        } else if (!empty($docPurchaseDeliver = $this->docPurchaseDeliverTable->getDocPurchaseDeliverMini($uuidRepl))) {
            $this->pdfPurchaseDeliver->setDocPurchaseDeliverNoCompl($docPurchaseDeliver['doc_purchase_deliver_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($entities = $this->computePurchaseOrderItemAttachEntities($items, $itemUuids))) {
            $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
            return '';
        }
        $this->pdfPurchaseDeliver->setPurchaseOrderItemAttachEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($supplier['supplier_name'])
                , html_entity_decode($supplier['supplier_street'])
                , html_entity_decode($supplier['supplier_zip'] . ' ' . $supplier['supplier_city'])
                , html_entity_decode($this->countryService->getCountryLabel($supplier['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfPurchaseDeliver->setBaseDataEntity($baseData);
        $this->pdfPurchaseDeliver->makeDocument();
        $path = $this->pdfPurchaseDeliver->writeDocument();
        if (!file_exists($path)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create PDF purchaseOrder for ' . $uuid);
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docPurchaseDeliverTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docPurchaseDeliverTable->updateDocPurchaseDeliver($uuidRepl, basename($path), $userUuid);
            if ($this->docPurchaseDeliverItemTable->deleteDocPurchaseDeliverItemsForDocPurchaseDeliverUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docPurchaseDeliverTable->insertDocPurchaseDeliver(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $uuid, $userUuid);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docPurchaseDeliverItemTable->insertDocPurchaseDeliverItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }

}
