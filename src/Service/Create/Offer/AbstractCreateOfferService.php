<?php

namespace Lerp\Document\Service\Create\Offer;

use Lerp\Document\Service\Create\AbstractCreateService;
use Lerp\Order\Entity\Offer\OfferItemEntity;
use Lerp\Order\Table\Offer\OfferItemTable;
use Lerp\Order\Table\Offer\OfferTable;

abstract class AbstractCreateOfferService extends AbstractCreateService
{
    protected OfferTable $offerTable;
    protected OfferItemTable $offerItemTable;

    public function setOfferTable(OfferTable $offerTable): void
    {
        $this->offerTable = $offerTable;
    }

    public function setOfferItemTable(OfferItemTable $offerItemTable): void
    {
        $this->offerItemTable = $offerItemTable;
    }

    /**
     * @param array $items
     * @param array $itemUuids
     * @return OfferItemEntity[]
     */
    protected function computeOfferItemEntities(array $items, array $itemUuids): array
    {
        $entities = [];
        foreach ($items as $item) {
            $e = new OfferItemEntity();
            $e->exchangeArrayFromDatabase($item);
            if ($itemUuids[$e->getUuid()] > $e->getOfferItemQuantity()) {
                $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
                return [];
            }
            $e->setOfferItemQuantity($itemUuids[$e->getUuid()]);
            $e->setOfferItemPriceTotal($itemUuids[$e->getUuid()] * $e->getOfferItemPrice());
            $entities[] = $e;
        }
        return $entities;
    }
}
