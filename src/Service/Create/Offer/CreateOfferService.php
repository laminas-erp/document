<?php

namespace Lerp\Document\Service\Create\Offer;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Entity\BaseDataEntity;
use Lerp\Document\Pdf\Concrete\OfferOfferProviderInterface;
use Lerp\Document\Pdf\DocumentProviderInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Offer\DocOfferItemTable;
use Lerp\Document\Table\Offer\DocOfferTable;

class CreateOfferService extends AbstractCreateOfferService
{
    protected DocumentProviderInterface|OfferOfferProviderInterface $pdfOffer;
    protected DocOfferTable $docOfferTable;
    protected DocOfferItemTable $docOfferItemTable;

    public function setPdfOffer(DocumentProviderInterface|OfferOfferProviderInterface $pdfOffer): void
    {
        $this->pdfOffer = $pdfOffer;
    }

    public function setDocOfferTable(DocOfferTable $docOfferTable): void
    {
        $this->docOfferTable = $docOfferTable;
    }

    public function setDocOfferItemTable(DocOfferItemTable $docOfferItemTable): void
    {
        $this->docOfferItemTable = $docOfferItemTable;
    }

    public function createOffer(string $offerUuid, array $itemUuids, string $start, string $foot, string $userUuid, string $uuidRepl): string
    {
        if (empty($offer = $this->offerTable->getOffer($offerUuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($itemUuids)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (
            empty($items = $this->offerItemTable->getOfferItemsWithUuids(array_keys($itemUuids)))
            || empty($customer = $this->customerService->getCustomer($offer['customer_uuid']))
            || empty($user = $this->equipmentUserTable->getUser($offer['user_uuid_create']))
            || empty($newDocNoCompl = $this->uniqueNumberProvider->computeGetNumberComplete(DocumentService::DOC_TYPE_OFFER))
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $this->translator->setLocale($this->langLocaleMap[$offer['offer_lang_iso']]);
        $isReplace = false;
        if (empty($uuidRepl)) {
            $this->pdfOffer->setDocOfferNoCompl($newDocNoCompl);
        } else if (!empty($docOffer = $this->docOfferTable->getDocOfferMini($uuidRepl))) {
            $this->pdfOffer->setDocOfferNoCompl($docOffer['doc_offer_no_compl']);
            $isReplace = true;
        } else {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        if (empty($entities = $this->computeOfferItemEntities($items, $itemUuids))) {
            $this->message = $this->translator->translate('amount_bigger_than_origin', 'lerp_doc');
            return '';
        }
        $this->pdfOffer->setOfferItemEntities($entities);
        $baseData = new BaseDataEntity();
        $baseData->setRecipientLines(
            [
                html_entity_decode($customer['customer_name'])
                , html_entity_decode($customer['customer_street'])
                , html_entity_decode($customer['customer_zip'] . ' ' . $customer['customer_city'])
                , html_entity_decode($this->countryService->getCountryLabel($customer['country_id']))
            ]
        );
        $baseData->setInfoboxOurSign($user['user_login']);
        $baseData->setInfoboxOurContact(html_entity_decode($user['user_details_name_first'] . ' ' . $user['user_details_name_last']));
        $baseData->setSalutation($this->translator->translate('salutation_ladies_gentlemen', 'lerp_doc'));
        $baseData->setContentStartHTML(nl2br($start, false));
        $baseData->setContentEndHTML(nl2br($foot, false));

        $this->pdfOffer->setBaseDataEntity($baseData);
        $this->pdfOffer->makeDocument();
        $priceSum = $this->pdfOffer->getPriceSum();
        $taxSum = $this->pdfOffer->getTaxSum();
        $path = $this->pdfOffer->writeDocument();
        if (!file_exists($path)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docOfferTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($isReplace) {
            $docUuid = $this->docOfferTable->updateDocOffer($uuidRepl, basename($path), $userUuid, $priceSum, $taxSum);
            if ($this->docOfferItemTable->deleteDocOfferItemsForDocOfferUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docOfferTable->insertDocOffer(basename($path), $this->uniqueNumberProvider->getNumber(), $newDocNoCompl, $offerUuid, $userUuid, $priceSum, $taxSum);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($entities as $entity) {
            if (empty($this->docOfferItemTable->insertDocOfferItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }

}
