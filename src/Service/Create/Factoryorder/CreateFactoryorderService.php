<?php

namespace Lerp\Document\Service\Create\Factoryorder;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Document\Pdf\DocumentSimpleProviderInterface;
use Lerp\Document\Pdf\Concrete\FactoryorderProviderInterface;
use Lerp\Document\Service\Create\AbstractCreateService;
use Lerp\Document\Table\Factoryorder\DocFactoryorderItemTable;
use Lerp\Document\Table\Factoryorder\DocFactoryorderTable;
use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Entity\FactoryorderWorkflowEntity;
use Lerp\Factoryorder\Table\FactoryorderTable;
use Lerp\Factoryorder\Table\FactoryorderWorkflowTable;

class CreateFactoryorderService extends AbstractCreateService
{
    protected DocumentSimpleProviderInterface|FactoryorderProviderInterface $pdfFactoryorder;
    protected FactoryorderTable $factoryorderTable;
    protected FactoryorderWorkflowTable $factoryorderWorkflowTable;
    protected DocFactoryorderTable $docFactoryorderTable;
    protected DocFactoryorderItemTable $docFactoryorderItemTable;

    public function setPdfFactoryorder(DocumentSimpleProviderInterface|FactoryorderProviderInterface $pdfFactoryorder): void
    {
        $this->pdfFactoryorder = $pdfFactoryorder;
    }

    public function setFactoryorderTable(FactoryorderTable $factoryorderTable): void
    {
        $this->factoryorderTable = $factoryorderTable;
    }

    public function setFactoryorderWorkflowTable(FactoryorderWorkflowTable $factoryorderWorkflowTable): void
    {
        $this->factoryorderWorkflowTable = $factoryorderWorkflowTable;
    }

    public function setDocFactoryorderTable(DocFactoryorderTable $docFactoryorderTable): void
    {
        $this->docFactoryorderTable = $docFactoryorderTable;
    }

    public function setDocFactoryorderItemTable(DocFactoryorderItemTable $docFactoryorderItemTable): void
    {
        $this->docFactoryorderItemTable = $docFactoryorderItemTable;
    }

    public function createFactoryorder(string $factoryorderUuid, string $userUuid, string $uuidRepl): string
    {
        if (empty($fo = $this->factoryorderTable->getFactoryorder($factoryorderUuid))) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        $foe = new FactoryorderEntity();
        if (!$foe->exchangeArrayFromDatabase($fo)) {
            return '';
        }
        $items = $this->factoryorderWorkflowTable->getFactoryorderWorkflows($factoryorderUuid);
        $foWfEntities = [];
        foreach ($items as $item) {
            $e = new FactoryorderWorkflowEntity();
            if (!$e->exchangeArrayFromDatabase($item)) {
                $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
            $foWfEntities[] = $e;
        }
        $this->pdfFactoryorder->setFactoryorder($foe);
        $this->pdfFactoryorder->setFactoryorderWorkflows($foWfEntities);
        $this->pdfFactoryorder->makeDocument();
        $path = $this->pdfFactoryorder->writeDocument();
        if (!file_exists($path)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        /*
         * ####### database #######
         */
        /** @var Adapter $adapter */
        $adapter = $this->docFactoryorderTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if (!empty($uuidRepl) && $this->docFactoryorderTable->existDocFactoryorder($uuidRepl)) {
            $docUuid = $this->docFactoryorderTable->updateDocFactoryorder($uuidRepl, basename($path), $userUuid);
            if ($this->docFactoryorderItemTable->deleteDocFactoryorderItemsForDocFactoryorderUuid($uuidRepl) < 0) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        } else {
            $docUuid = $this->docFactoryorderTable->insertDocFactoryorder(basename($path), $factoryorderUuid, $userUuid);
        }
        if (empty($docUuid)) {
            $connection->rollback();
            unlink($path);
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            return '';
        }
        foreach ($foWfEntities as $entity) {
            if (empty($this->docFactoryorderItemTable->insertDocFactoryorderItem($docUuid, $entity))) {
                $connection->rollback();
                unlink($path);
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
                return '';
            }
        }
        $connection->commit();
        return $path;
    }
}
