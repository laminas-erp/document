<?php

namespace Lerp\Document\Service\Create;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\I18n\Translator\Translator;
use Lerp\Common\Service\Lists\CountryService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Customer\Service\CustomerService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Unique\UniqueNumberProviderInterface;
use Lerp\Equipment\Table\User\EquipmentUserTable;

abstract class AbstractCreateService extends AbstractService
{
    protected array $langLocaleMap;
    protected DocumentService $documentService;
    protected Translator $translator;
    protected CustomerService $customerService;
    protected EquipmentUserTable $equipmentUserTable;
    protected CountryService $countryService;
    protected UniqueNumberProviderInterface $uniqueNumberProvider;
    protected QuantityUnitService $quantityUnitService;

    public function setLangLocaleMap(array $langLocaleMap): void
    {
        $this->langLocaleMap = $langLocaleMap;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    /**
     * @param Translator $translator DocumentTranslator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    public function setCustomerService(CustomerService $customerService): void
    {
        $this->customerService = $customerService;
    }

    public function setEquipmentUserTable(EquipmentUserTable $equipmentUserTable): void
    {
        $this->equipmentUserTable = $equipmentUserTable;
    }

    public function setCountryService(CountryService $countryService): void
    {
        $this->countryService = $countryService;
    }

    public function setUniqueNumberProvider(UniqueNumberProviderInterface $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    public function setQuantityUnitService(QuantityUnitService $quantityUnitService): void
    {
        $this->quantityUnitService = $quantityUnitService;
    }

    /**
     * @param string $docType One of the constants in DocumentService::DOC_TYPE_*
     * @return array Array with table and tablefield names: ['table' => '', 'field' => '']
     * Use this function in controllers to fetch table- and field-name for \Lerp\Document\Form\DocCreateForm
     */
    public function getTableAndPrimaryKeyFor(string $docType): array
    {
        if (!in_array($docType, $this->documentService->getDocTypes())) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' No docType for docType: ' . $docType);
            return [];
        }
        $arr = ['table' => '', 'field' => ''];
        switch ($docType) {
            case DocumentService::DOC_TYPE_ORDER_CONFIRM:
                $arr['table'] = 'doc_order_confirm';
                $arr['field'] = 'doc_order_confirm_uuid';
        }
        return $arr;
    }

}
