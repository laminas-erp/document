<?php

namespace Lerp\Document\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;

class DocumentService extends AbstractService
{

    /**
     * Bestellung (Einkauf)
     */
    const DOC_TYPE_PURCHASEORDER = 'purchaseOrder';

    /**
     * Lieferschein einer Bestellung (z.B. Teile zum Lackieren, wenn Lackieren das bestellte Produkt ist)
     */
    const DOC_TYPE_PURCHASEORDER_DELIVERY = 'purchaseOrderDelivery';
    /**
     * Anfrage (Einkauf)
     */
    const DOC_TYPE_PURCHASEORDER_REQUEST = 'purchaseOrderRequest';

    /**
     * Angebot
     */
    const DOC_TYPE_OFFER = 'offer';

    /**
     * kostenvoranschlag im Auftrag
     */
    const DOC_TYPE_ESTIMATE = 'estimate';

    /**
     * Auftragsbestaetigung
     */
    const DOC_TYPE_ORDER_CONFIRM = 'orderConfirm';

    /**
     * Rechnung
     */
    const DOC_TYPE_INVOICE = 'invoice';

    /**
     * Proforma Rechnung
     */
    const DOC_TYPE_PROFORMA = 'proforma';

    /**
     * Lieferschein eines Auftrags
     */
    const DOC_TYPE_ORDER_DELIVERY = 'delivery';

    const FILENAME_ESTIMATE = 'Kostenvoranschlag.pdf';
    const FILENAME_OFFER_OFFER = 'Angebot.pdf';
    const FILENAME_ORDER_CONFIRM = 'Auftragsbestaetigung.pdf';
    const FILENAME_ORDER_DELIVERY = 'Lieferschein.pdf';
    const FILENAME_INVOICE = 'Rechnung.pdf';
    const FILENAME_PROFORMA = 'ProformaRechnung.pdf';
    const FILENAME_PURCHASEORDER = 'Bestellung.pdf';
    const FILENAME_PURCHASEORDER_DELIVERY = 'LieferscheinBestellung.pdf';
    const FILENAME_PURCHASEORDER_REQUEST = 'AnfrageBestellung.pdf';

    /**
     * @var array List of doc types with their associated folder name.
     */
    protected array $docTypes = [
        self::DOC_TYPE_PURCHASEORDER                       => 'purchaseOrder',
        self::DOC_TYPE_PURCHASEORDER_DELIVERY              => 'purchaseOrderDelivery',
        self::DOC_TYPE_PURCHASEORDER_REQUEST               => 'purchaseOrderRequest',
        self::DOC_TYPE_OFFER                               => 'offer',
        self::DOC_TYPE_ESTIMATE                            => 'estimate',
        self::DOC_TYPE_ORDER_CONFIRM                       => 'orderConfirm',
        self::DOC_TYPE_INVOICE                             => 'invoice',
        self::DOC_TYPE_PROFORMA                            => 'proforma',
        self::DOC_TYPE_ORDER_DELIVERY                      => 'delivery',
        FactoryorderDocumentService::DOC_TYPE_FACTORYORDER => 'factoryOrder'
    ];

    protected string $storageLocationPdf;
    protected FolderTool $folderTool;

    public function getDocTypes(): array
    {
        return $this->docTypes;
    }

    public function setStorageLocationPdf(string $storageLocationPdf): void
    {
        $this->storageLocationPdf = $storageLocationPdf;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $docType
     * @param int $unixtime
     * @param string $id
     * @return string
     */
    public function computeDocumentFolder(string $rootFolderAbsolute, string $docType, int $unixtime, string $id): string
    {
        if (!array_key_exists($docType, $this->docTypes)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Doctype ' . $docType . ' does not exist.');
        }
        $rootFolderAbsolute = realpath($rootFolderAbsolute) . DIRECTORY_SEPARATOR . $this->docTypes[$docType];
        $this->folderTool->computeFolder($rootFolderAbsolute);
        return $this->folderTool->computeDateIdFolder(
            $rootFolderAbsolute,
            date('Y', $unixtime),
            date('m', $unixtime),
            $id
        );
    }

    /**
     * @param string $docType
     * @param int $unixtime
     * @param string $id
     * @return string
     */
    public function getDocumentFolder(string $docType, int $unixtime, string $id): string
    {
        if (!array_key_exists($docType, $this->docTypes)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Doctype ' . $docType . ' does not exist.');
        }
        return $this->docTypes[$docType] . DIRECTORY_SEPARATOR . date('Y', $unixtime) . DIRECTORY_SEPARATOR . date('m', $unixtime) . DIRECTORY_SEPARATOR . $id;
    }

    /**
     * @param string $docType
     * @param int $unixtime
     * @param string $id
     * @return string
     */
    public function getDocumentFolderAbsolute(string $docType, int $unixtime, string $id): string
    {
        return $this->storageLocationPdf . DIRECTORY_SEPARATOR . $this->getDocumentFolder($docType, $unixtime, $id);
    }
}
