<?php

namespace Lerp\Document\Table\Offer;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocOfferSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_offer_send';

    protected static string $selectInto = 'INSERT INTO doc_offer_send
SELECT :docOfferSendUuid,
       doc_offer_uuid,
       doc_offer_filename,
       doc_offer_price,
       doc_offer_tax,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendOfferRelUuid
FROM doc_offer
WHERE doc_offer_uuid = :docOfferUuid';

    public function selectIntoSend(string $docOfferUuid, string $userUuid, string $emails, string $mailSendOfferRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docOfferSendUuid'       => $uuid
            , ':userUuidSend'         => $userUuid
            , ':docOfferUuid'         => $docOfferUuid
            , ':docEmails'            => $emails
            , ':mailSendOfferRelUuid' => $mailSendOfferRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docOfferSendUuid
     * @return array
     */
    public function getDocOfferSend(string $docOfferSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_offer_send_uuid' => $docOfferSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
