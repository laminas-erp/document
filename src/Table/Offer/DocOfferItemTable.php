<?php

namespace Lerp\Document\Table\Offer;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Offer\OfferItemEntity;

class DocOfferItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_offer_item';

    /**
     * @param string $docOfferItemUuid
     * @return array
     */
    public function getDocOfferItem(string $docOfferItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_offer_item_uuid' => $docOfferItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDocOfferItemsByOfferItemUuid(string $offerItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['offer_item_uuid' => $offerItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteDocOfferItemsForDocOfferUuid(string $docOfferUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_offer_uuid' => $docOfferUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocOfferItem(string $docOfferUuid, OfferItemEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_offer_item_uuid' => $uuid,
                'doc_offer_uuid' => $docOfferUuid,
                'offer_item_uuid' => $entity->getUuid(),
                'offer_item_text_short' => $entity->getOfferItemTextShort(),
                'offer_item_text_long' => $entity->getOfferItemTextLong(),
                'offer_item_quantity' => $entity->getOfferItemQuantity(),
                'quantityunit_uuid' => $entity->getQuantityunitUuid(),
                'offer_item_price' => $entity->getOfferItemPrice(),
                'offer_item_price_total' => $entity->getOfferItemPriceTotal(),
                'offer_item_taxp' => $entity->getOfferItemTaxp(),
                'cost_centre_id' => $entity->getCostCentreId(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
