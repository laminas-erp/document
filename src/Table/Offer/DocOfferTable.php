<?php

namespace Lerp\Document\Table\Offer;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocOfferTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_offer';
    protected string $viewDocOffer = 'view_doc_offer';

    /**
     * @param string $docOfferUuid
     * @return array
     */
    public function getDocOfferMini(string $docOfferUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_offer_uuid' => $docOfferUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocOffer(string $docOfferUuid): bool
    {
        return !empty($this->getDocOfferMini($docOfferUuid));
    }

    /**
     * @param string $docOfferUuid
     * @return array From db.view_doc_offer
     */
    public function getDocOffer(string $docOfferUuid): array
    {
        $select = new Select($this->viewDocOffer);
        try {
            $select->where(['doc_offer_uuid' => $docOfferUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocOffer(string $filename, int $docOfferNo, string $docOfferNoCompl, string $offerUuid, string $userUuid, float $price, float $tax): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_offer_uuid'     => $uuid,
                'doc_offer_filename' => $filename,
                'doc_offer_no'       => $docOfferNo,
                'doc_offer_no_compl' => $docOfferNoCompl,
                'offer_uuid'         => $offerUuid,
                'user_uuid_create'   => $userUuid,
                'doc_offer_price'    => $price,
                'doc_offer_tax'      => $tax,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocOffer(string $docOfferUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_offer_time_replace' => new Expression('current_timestamp'),
                'doc_offer_filename'     => $filename,
                'user_uuid_replace'      => $userUuid,
                'doc_offer_price'        => $price,
                'doc_offer_tax'          => $tax,
            ]);
            $update->where(['doc_offer_uuid' => $docOfferUuid]);
            if ($this->updateWith($update) > 0) {
                return $docOfferUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocOfferNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_offer_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocOfferNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_offer_no)')]);
            $select->where->greaterThanOrEqualTo('doc_offer_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_offer_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $offerUuid
     * @return array From db.view_doc_offer
     */
    public function getDocOffersForOffer(string $offerUuid): array
    {
        $select = new Select($this->viewDocOffer);
        try {
            $select->order('doc_offer_time_create DESC');
            $select->where(['offer_uuid' => $offerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_doc_offer
     */
    public function getDocOffersForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocOffer);
        try {
            $select->order('doc_offer_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
