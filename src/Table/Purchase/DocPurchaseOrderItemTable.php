<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Purchase\Entity\PurchaseOrderItemEntity;

class DocPurchaseOrderItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_order_item';

    /**
     * @param string $docPurchaseOrderItemUuid
     * @return array
     */
    public function getDocPurchaseOrderItem(string $docPurchaseOrderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_order_item_uuid' => $docPurchaseOrderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $docPurchaseOrderUuid
     * @param PurchaseOrderItemEntity $entity
     * @return string
     */
    public function insertDocPurchaseOrderItem(string $docPurchaseOrderUuid, PurchaseOrderItemEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_purchase_order_item_uuid'    => $uuid,
                'doc_purchase_order_uuid'         => $docPurchaseOrderUuid,
                'purchase_order_item_uuid'        => $entity->getUuid(),
                'purchase_order_item_text_short'  => $entity->getPurchaseOrderItemTextShort(),
                'purchase_order_item_text_long'   => $entity->getPurchaseOrderItemTextLong(),
                'purchase_order_item_quantity'    => $entity->getPurchaseOrderItemQuantity(),
                'quantityunit_uuid'               => $entity->getQuantityunitUuid(),
                'purchase_order_item_price'       => $entity->getPurchaseOrderItemPrice(),
                'purchase_order_item_price_total' => $entity->getPurchaseOrderItemPriceTotal(),
                'purchase_order_item_taxp'        => $entity->getPurchaseOrderItemTaxp(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteDocPurchaseOrderItemsForDocPurchaseOrderUuid(string $docPurchaseOrderUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_purchase_order_uuid' => $docPurchaseOrderUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getDocPurchaseOrderItemsByPurchaseOrderItemUuid(string $purchaseOrderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array From `view_doc_purchase_order_item`.
     */
    public function getDocPurchaseOrderItemsForProduct(string $productUuid): array
    {
        $select = new Select('view_doc_purchase_order_item');
        try {
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
