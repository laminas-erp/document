<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocPurchaseRequestSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_request_send';

    protected static string $selectInto = 'INSERT INTO doc_purchase_request_send
SELECT :docPurchaseRequestSendUuid,
       doc_purchase_request_uuid,
       doc_purchase_request_filename,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendPurchaseRequestRelUuid
FROM doc_purchase_request
WHERE doc_purchase_request_uuid = :docPurchaseRequestUuid';

    public function selectIntoSend(string $docPurchaseRequestUuid, string $userUuid, string $emails, string $mailSendPurchaseRequestRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docPurchaseRequestSendUuid'       => $uuid
            , ':userUuidSend'                   => $userUuid
            , ':docPurchaseRequestUuid'         => $docPurchaseRequestUuid
            , ':docEmails'                      => $emails
            , ':mailSendPurchaseRequestRelUuid' => $mailSendPurchaseRequestRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docPurchaseRequestSendUuid
     * @return array
     */
    public function getDocPurchaseRequestSend(string $docPurchaseRequestSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_request_send_uuid' => $docPurchaseRequestSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
