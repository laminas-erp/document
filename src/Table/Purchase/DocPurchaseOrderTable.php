<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocPurchaseOrderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_order';
    protected string $viewDocPurchaseOrder = 'view_doc_purchase_order';

    public function getDocPurchaseOrderMini(string $docPurchaseOrderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                Select::SQL_STAR,
                'doc_purchase_order_time_create_unix' => new Expression('extract(EPOCH from date_trunc(\'second\', doc_purchase_order_time_create)::timestamp(0))')
            ]);
            $select->where(['doc_purchase_order_uuid' => $docPurchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocPurchaseOrder(string $docPurchaseOrderUuid): bool
    {
        return !empty($this->getDocPurchaseOrderMini($docPurchaseOrderUuid));
    }

    /**
     * @param string $docPurchaseOrderUuid
     * @return array From db.view_doc_purchase_order
     */
    public function getDocPurchaseOrder(string $docPurchaseOrderUuid): array
    {
        $select = new Select($this->viewDocPurchaseOrder);
        try {
            $select->where(['doc_purchase_order_uuid' => $docPurchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getMaxDocPurchaseOrderNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_purchase_order_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocPurchaseOrderNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_purchase_order_no)')]);
            $select->where->greaterThanOrEqualTo('doc_purchase_order_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_purchase_order_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocPurchaseOrder(string $filename, int $docPurchaseOrderNo, string $docPurchaseOrderNoCompl, string $poUuid, string $userUuid, float $price, float $tax): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_purchase_order_uuid'     => $uuid,
                'doc_purchase_order_filename' => $filename,
                'doc_purchase_order_no'       => $docPurchaseOrderNo,
                'doc_purchase_order_no_compl' => $docPurchaseOrderNoCompl,
                'purchase_order_uuid'         => $poUuid,
                'user_uuid_create'            => $userUuid,
                'doc_purchase_order_price'    => $price,
                'doc_purchase_order_tax'      => $tax,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array From db.view_doc_purchase_order
     */
    public function getDocPurchaseOrdersForPurchaseOrder(string $purchaseOrderUuid): array
    {
        $select = new Select($this->viewDocPurchaseOrder);
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            $select->order('doc_purchase_order_time_create DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateDocPurchaseOrder(string $docPurchaseOrderUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_purchase_order_time_replace' => new Expression('current_timestamp'),
                'doc_purchase_order_filename'     => $filename,
                'user_uuid_replace'               => $userUuid,
                'doc_purchase_order_price'        => $price,
                'doc_purchase_order_tax'          => $tax,
            ]);
            $update->where(['doc_purchase_order_uuid' => $docPurchaseOrderUuid]);
            if ($this->updateWith($update) >= 0) {
                return $docPurchaseOrderUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
