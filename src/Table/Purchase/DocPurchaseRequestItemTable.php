<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Purchase\Entity\PurchaseRequestItemEntity;

class DocPurchaseRequestItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_request_item';

    /**
     * @param string $docPurchaseRequestItemUuid
     * @return array
     */
    public function getDocPurchaseRequestItem(string $docPurchaseRequestItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_request_item_uuid' => $docPurchaseRequestItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $docPurchaseRequestUuid
     * @param PurchaseRequestItemEntity $entity
     * @return string
     */
    public function insertDocPurchaseRequestItem(string $docPurchaseRequestUuid, PurchaseRequestItemEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_purchase_request_item_uuid' => $uuid,
                'doc_purchase_request_uuid' => $docPurchaseRequestUuid,
                'purchase_request_item_uuid' => $entity->getUuid(),
                'purchase_request_item_text_short' => $entity->getPurchaseRequestItemTextShort(),
                'purchase_request_item_text_long' => $entity->getPurchaseRequestItemTextLong(),
                'purchase_request_item_quantity' => $entity->getPurchaseRequestItemQuantity(),
                'quantityunit_uuid' => $entity->getQuantityunitUuid(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteDocPurchaseRequestItemsForDocPurchaseRequestUuid(string $docPurchaseRequestUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_purchase_request_uuid' => $docPurchaseRequestUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getDocPurchaseRequestItemsByPurchaseRequestItemUuid(string $purchaseRequestItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_request_item_uuid' => $purchaseRequestItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
