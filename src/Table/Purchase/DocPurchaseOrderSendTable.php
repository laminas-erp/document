<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocPurchaseOrderSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_order_send';

    protected static string $selectInto = 'INSERT INTO doc_purchase_order_send
SELECT :docPurchaseOrderSendUuid,
       doc_purchase_order_uuid,
       doc_purchase_order_filename,
       doc_purchase_order_price,
       doc_purchase_order_tax,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendPurchaseOrderRelUuid
FROM doc_purchase_order
WHERE doc_purchase_order_uuid = :docPurchaseOrderUuid';

    /**
     * @param string $docPurchaseOrderUuid
     * @param string $userUuid
     * @param string $emails
     * @param string $mailSendPurchaseOrderRelUuid
     * @return bool
     */
    public function selectIntoSend(string $docPurchaseOrderUuid, string $userUuid, string $emails, string $mailSendPurchaseOrderRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docPurchaseOrderSendUuid'       => $uuid
            , ':userUuidSend'                 => $userUuid
            , ':docPurchaseOrderUuid'         => $docPurchaseOrderUuid
            , ':docEmails'                    => $emails
            , ':mailSendPurchaseOrderRelUuid' => $mailSendPurchaseOrderRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docPurchaseOrderSendUuid
     * @return array
     */
    public function getDocPurchaseOrderSend(string $docPurchaseOrderSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_order_send_uuid' => $docPurchaseOrderSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
