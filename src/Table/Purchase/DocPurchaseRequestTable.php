<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocPurchaseRequestTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_request';
    protected string $viewDocPurchaseRequest = 'view_doc_purchase_request';

    public function getDocPurchaseRequestMini(string $docPurchaseRequestUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                Select::SQL_STAR,
                'doc_purchase_request_time_create_unix' => new Expression('extract(EPOCH from doc_purchase_request_time_create::timestamp(0))')
            ]);
            $select->where(['doc_purchase_request_uuid' => $docPurchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocPurchaseRequest(string $docPurchaseRequestUuid): bool
    {
        return !empty($this->getDocPurchaseRequestMini($docPurchaseRequestUuid));
    }

    /**
     * @param string $docPurchaseRequestUuid
     * @return array From db.view_doc_purchase_request
     */
    public function getDocPurchaseRequest(string $docPurchaseRequestUuid): array
    {
        $select = new Select($this->viewDocPurchaseRequest);
        try {
            $select->where(['doc_purchase_request_uuid' => $docPurchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getMaxDocPurchaseRequestNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_purchase_request_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocPurchaseRequestNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_purchase_request_no)')]);
            $select->where->greaterThanOrEqualTo('doc_purchase_request_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_purchase_request_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocPurchaseRequest(string $filename, int $docPurchaseRequestNo, string $docPurchaseRequestNoCompl, string $prUuid, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_purchase_request_uuid'     => $uuid,
                'doc_purchase_request_filename' => $filename,
                'doc_purchase_request_no'       => $docPurchaseRequestNo,
                'doc_purchase_request_no_compl' => $docPurchaseRequestNoCompl,
                'purchase_request_uuid'         => $prUuid,
                'user_uuid_create'              => $userUuid
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseRequestUuid
     * @return array From db.view_doc_purchase_request
     */
    public function getDocPurchaseRequestsForPurchaseRequest(string $purchaseRequestUuid): array
    {
        $select = new Select($this->viewDocPurchaseRequest);
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            $select->order('doc_purchase_request_time_create DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateDocPurchaseRequest(string $docPurchaseRequestUuid, string $filename, string $userUuid): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_purchase_request_time_replace' => new Expression('current_timestamp'),
                'doc_purchase_request_filename'     => $filename,
                'user_uuid_replace'                 => $userUuid,
            ]);
            $update->where(['doc_purchase_request_uuid' => $docPurchaseRequestUuid]);
            if ($this->updateWith($update) >= 0) {
                return $docPurchaseRequestUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
