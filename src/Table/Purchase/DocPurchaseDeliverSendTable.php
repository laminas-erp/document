<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocPurchaseDeliverSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_deliver_send';

    protected static string $selectInto = 'INSERT INTO doc_purchase_deliver_send
SELECT :docPurchaseDeliverSendUuid,
       doc_purchase_deliver_uuid,
       doc_purchase_deliver_filename,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendPurchaseOrderRelUuid
FROM doc_purchase_deliver
WHERE doc_purchase_deliver_uuid = :docPurchaseDeliverUuid';

    public function selectIntoSend(string $docPurchaseDeliverUuid, string $userUuid, string $emails, string $mailSendPurchaseOrderRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docPurchaseDeliverSendUuid'     => $uuid
            , ':userUuidSend'                 => $userUuid
            , ':docPurchaseDeliverUuid'       => $docPurchaseDeliverUuid
            , ':docEmails'                    => $emails
            , ':mailSendPurchaseOrderRelUuid' => $mailSendPurchaseOrderRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docPurchaseDeliverSendUuid
     * @return array
     */
    public function getDocPurchaseDeliverSend(string $docPurchaseDeliverSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_deliver_send_uuid' => $docPurchaseDeliverSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
