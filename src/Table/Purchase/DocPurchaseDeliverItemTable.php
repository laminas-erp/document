<?php

namespace Lerp\Document\Table\Purchase;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Purchase\Entity\PurchaseOrderItemAttachEntity;

class DocPurchaseDeliverItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_purchase_deliver_item';

    /**
     * @param string $docPurchaseDeliverItemUuid
     * @return array
     */
    public function getDocPurchaseDeliverItem(string $docPurchaseDeliverItemUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_purchase_deliver_item_uuid' => $docPurchaseDeliverItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $docPurchaseDeliverUuid
     * @param PurchaseOrderItemAttachEntity $entity
     * @return string
     */
    public function insertDocPurchaseDeliverItem(string $docPurchaseDeliverUuid, PurchaseOrderItemAttachEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_purchase_deliver_item_uuid' => $uuid,
                'doc_purchase_deliver_uuid' => $docPurchaseDeliverUuid,
                'purchase_order_item_attach_uuid' => $entity->getUuid(),
                'purchase_order_item_attach_text_short' => $entity->getPurchaseOrderItemAttachTextShort(),
                'purchase_order_item_attach_text_long' => $entity->getPurchaseOrderItemAttachTextLong(),
                'purchase_order_item_attach_quantity' => $entity->getPurchaseOrderItemAttachQuantity(),
                'quantityunit_uuid' => $entity->getQuantityunitUuid(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteDocPurchaseDeliverItemsForDocPurchaseDeliverUuid(string $docPurchaseDeliverUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_purchase_deliver_uuid' => $docPurchaseDeliverUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getDocPurchaseDeliverItemsByPurchaseOrderItemUuid(string $purchaseOrderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
