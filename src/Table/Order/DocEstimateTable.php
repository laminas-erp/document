<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocEstimateTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_estimate';
    protected string $viewDocEstimate = 'view_doc_estimate';

    /**
     * @param string $docEstimateUuid
     * @return array
     */
    public function getDocEstimateMini(string $docEstimateUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_estimate_uuid' => $docEstimateUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocEstimate(string $docEstimateUuid): bool
    {
        return !empty($this->getDocEstimateMini($docEstimateUuid));
    }

    /**
     * @param string $docEstimateUuid
     * @return array From db.view_doc_estimate
     */
    public function getDocEstimate(string $docEstimateUuid): array
    {
        $select = new Select($this->viewDocEstimate);
        try {
            $select->where(['doc_estimate_uuid' => $docEstimateUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocEstimate(string $filename, int $docEstimateNo, string $docEstimateNoCompl, string $orderUuid, string $userUuid, float $price, float $tax): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_estimate_uuid'     => $uuid,
                'doc_estimate_filename' => $filename,
                'doc_estimate_no'       => $docEstimateNo,
                'doc_estimate_no_compl' => $docEstimateNoCompl,
                'order_uuid'            => $orderUuid,
                'user_uuid_create'      => $userUuid,
                'doc_estimate_price'    => $price,
                'doc_estimate_tax'      => $tax,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocEstimate(string $docEstimateUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_estimate_time_replace' => new Expression('current_timestamp'),
                'doc_estimate_filename'     => $filename,
                'user_uuid_replace'         => $userUuid,
                'doc_estimate_price'        => $price,
                'doc_estimate_tax'          => $tax,
            ]);
            $update->where(['doc_estimate_uuid' => $docEstimateUuid]);
            if ($this->updateWith($update) > 0) {
                return $docEstimateUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocEstimateNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_estimate_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocEstimateNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_estimate_no)')]);
            $select->where->greaterThanOrEqualTo('doc_estimate_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_estimate_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_doc_estimate
     */
    public function getDocEstimatesForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocEstimate);
        try {
            $select->order('doc_estimate_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
