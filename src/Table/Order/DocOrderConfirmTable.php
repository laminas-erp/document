<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocOrderConfirmTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_order_confirm';
    protected string $viewDocOrderConfirm = 'view_doc_order_confirm';

    public function getDocOrderConfirmMini(string $docOrderConfirmUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_order_confirm_uuid' => $docOrderConfirmUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocOrderConfirm(string $docOrderConfirmUuid): bool
    {
        return !empty($this->getDocOrderConfirmMini($docOrderConfirmUuid));
    }

    /**
     * @param string $docOrderConfirmUuid
     * @return array From db.view_doc_order_confirm
     */
    public function getDocOrderConfirm(string $docOrderConfirmUuid): array
    {
        $select = new Select($this->viewDocOrderConfirm);
        try {
            $select->where(['doc_order_confirm_uuid' => $docOrderConfirmUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocOrderConfirm(string $filename, int $docOrderConfirmNo, string $docOrderConfirmNoCompl, string $orderUuid, string $userUuid, float $price, float $tax): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_order_confirm_uuid'     => $uuid,
                'doc_order_confirm_filename' => $filename,
                'doc_order_confirm_no'       => $docOrderConfirmNo,
                'doc_order_confirm_no_compl' => $docOrderConfirmNoCompl,
                'order_uuid'                 => $orderUuid,
                'user_uuid_create'           => $userUuid,
                'doc_order_confirm_price'    => $price,
                'doc_order_confirm_tax'      => $tax,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocOrderConfirm(string $docOrderConfirmUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_order_confirm_time_replace' => new Expression('current_timestamp'),
                'doc_order_confirm_filename'     => $filename,
                'user_uuid_replace'              => $userUuid,
                'doc_order_confirm_price'        => $price,
                'doc_order_confirm_tax'          => $tax,
            ]);
            $update->where(['doc_order_confirm_uuid' => $docOrderConfirmUuid]);
            if ($this->updateWith($update) >= 0) {
                return $docOrderConfirmUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocOrderConfirmNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_order_confirm_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocOrderConfirmNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_order_confirm_no)')]);
            $select->where->greaterThanOrEqualTo('doc_order_confirm_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_order_confirm_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_doc_order_confirm
     */
    public function getDocOrderConfirmsForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocOrderConfirm);
        try {
            $select->order('doc_order_confirm_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
