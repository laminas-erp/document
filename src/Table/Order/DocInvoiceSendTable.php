<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocInvoiceSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_invoice_send';

    protected static string $selectInto = 'INSERT INTO doc_invoice_send
SELECT :docInvoiceSendUuid,
       doc_invoice_uuid,
       doc_invoice_filename,
       doc_invoice_price,
       doc_invoice_tax,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendOrderRelUuid
FROM doc_invoice
WHERE doc_invoice_uuid = :docInvoiceUuid';

    public function selectIntoSend(string $docInvoiceUuid, string $userUuid, string $emails, string $mailSendOrderRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docInvoiceSendUuid'     => $uuid
            , ':userUuidSend'         => $userUuid
            , ':docInvoiceUuid'       => $docInvoiceUuid
            , ':docEmails'            => $emails
            , ':mailSendOrderRelUuid' => $mailSendOrderRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docInvoiceSendUuid
     * @return array
     */
    public function getDocInvoiceSend(string $docInvoiceSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_invoice_send_uuid' => $docInvoiceSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
