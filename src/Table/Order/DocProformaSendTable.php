<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocProformaSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_proforma_send';

    protected static string $selectInto = 'INSERT INTO doc_proforma_send
SELECT :docProformaSendUuid,
       doc_proforma_uuid,
       doc_proforma_filename,
       doc_proforma_price,
       doc_proforma_tax,
       current_timestamp,
       :userUuidSend,
       :docEmails
FROM doc_proforma
WHERE doc_proforma_uuid = :docProformaUuid';

    /**
     * @param string $docProformaSendUuid
     * @return array
     */
    public function getDocProformaSend(string $docProformaSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_proforma_send_uuid' => $docProformaSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function selectIntoSend(string $docProformaUuid, string $userUuid, string $emails): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docProformaSendUuid' => $uuid
            , ':userUuidSend' => $userUuid
            , ':docProformaUuid' => $docProformaUuid
            , ':docEmails' => $emails
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }
}
