<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocInvoiceTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_invoice';
    protected string $viewDocInvoice = 'view_doc_invoice';

    public function getDocInvoiceMini(string $docInvoiceUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_invoice_uuid' => $docInvoiceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocInvoice(string $docInvoiceUuid): bool
    {
        return !empty($this->getDocInvoiceMini($docInvoiceUuid));
    }

    /**
     * @param string $docInvoiceUuid
     * @return array
     */
    public function getDocInvoice(string $docInvoiceUuid): array
    {
        $select = new Select($this->viewDocInvoice);
        try {
            $select->where(['doc_invoice_uuid' => $docInvoiceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocInvoice(string $filename, int $docInvoiceNo, string $docInvoiceNoCompl, string $orderUuid, string $userUuid, float $price, float $tax, string $type): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_invoice_uuid'     => $uuid,
                'doc_invoice_filename' => $filename,
                'doc_invoice_no'       => $docInvoiceNo,
                'doc_invoice_no_compl' => $docInvoiceNoCompl,
                'order_uuid'           => $orderUuid,
                'user_uuid_create'     => $userUuid,
                'doc_invoice_price'    => $price,
                'doc_invoice_tax'      => $tax,
                'doc_invoice_type'     => $type,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocInvoice(string $docInvoiceUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_invoice_time_replace' => new Expression('current_timestamp'),
                'doc_invoice_filename'     => $filename,
                'user_uuid_replace'        => $userUuid,
                'doc_invoice_price'        => $price,
                'doc_invoice_tax'          => $tax,
            ]);
            $update->where(['doc_invoice_uuid' => $docInvoiceUuid]);
            if ($this->updateWith($update) > 0) {
                return $docInvoiceUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocInvoiceNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_invoice_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocInvoiceNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_invoice_no)')]);
            $select->where->greaterThanOrEqualTo('doc_invoice_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_invoice_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getDocInvoicesForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocInvoice);
        try {
            $select->order('doc_invoice_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
