<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocOrderConfirmSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_order_confirm_send';

    protected static string $selectInto = 'INSERT INTO doc_order_confirm_send
SELECT :docOrderConfirmSendUuid,
       doc_order_confirm_uuid,
       doc_order_confirm_filename,
       doc_order_confirm_price,
       doc_order_confirm_tax,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendOrderRelUuid
FROM doc_order_confirm
WHERE doc_order_confirm_uuid = :docOrderConfirmUuid';

    public function selectIntoSend(string $docOrderConfirmUuid, string $userUuid, string $emails, string $mailSendOrderRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docOrderConfirmSendUuid' => $uuid
            , ':userUuidSend'          => $userUuid
            , ':docOrderConfirmUuid'   => $docOrderConfirmUuid
            , ':docEmails'             => $emails
            , ':mailSendOrderRelUuid'  => $mailSendOrderRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docOrderConfirmItemUuid
     * @return array
     */
    public function getDocOrderConfirmItem(string $docOrderConfirmItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_order_confirm_item_uuid' => $docOrderConfirmItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
