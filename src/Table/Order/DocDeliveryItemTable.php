<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemEntity;

class DocDeliveryItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_delivery_item';

    /**
     * @param string $docDeliveryItemUuid
     * @return array
     */
    public function getDocDeliveryItem(string $docDeliveryItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_delivery_item_uuid' => $docDeliveryItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDocDeliveryItemsByOrderItemUuid(string $orderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteDocDeliveryItemsForDocDeliveryUuid(string $docDeliveryUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_delivery_uuid' => $docDeliveryUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocDeliveryItem(string $docDeliveryUuid, OrderItemEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_delivery_item_uuid' => $uuid,
                'doc_delivery_uuid'      => $docDeliveryUuid,
                'order_item_uuid'        => $entity->getUuid(),
                'order_item_text_short'  => $entity->getOrderItemTextShort(),
                'order_item_text_long'   => $entity->getOrderItemTextLong(),
                'order_item_quantity'    => $entity->getOrderItemQuantity(),
                'quantityunit_uuid'      => $entity->getQuantityunitUuid(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $docDeliveryUuid
     * @param bool $asAssoc
     * @return array From view_doc_delivery_item
     */
    public function getDocDeliveryItemsByDocDeliveryUuid(string $docDeliveryUuid, bool $asAssoc = true): array
    {
        $select = new Select('view_doc_delivery_item');
        try {
            $select->where(['doc_delivery_uuid' => $docDeliveryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($asAssoc) {
                    $assoc = [];
                    do {
                        $current = $result->current();
                        $assoc[$current['order_item_uuid']] = $current;
                        $result->next();
                    } while ($result->valid());
                    return $assoc;
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
