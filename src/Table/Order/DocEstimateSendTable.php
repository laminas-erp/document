<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class DocEstimateSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_estimate_send';

    protected static string $selectInto = 'INSERT INTO doc_estimate_send
SELECT :docEstimateSendUuid,
       doc_estimate_uuid,
       doc_estimate_filename,
       doc_estimate_price,
       doc_estimate_tax,
       CURRENT_TIMESTAMP,
       :userUuidSend,
       :docEmails,
       :mailSendOrderRelUuid
FROM doc_estimate
WHERE doc_estimate_uuid = :docEstimateUuid';

    public function selectIntoSend(string $docEstimateUuid, string $userUuid, string $emails, string $mailSendOrderRelUuid): bool
    {
        $uuid = $this->uuid();
        $params = new ParameterContainer([
            ':docEstimateSendUuid'    => $uuid
            , ':userUuidSend'         => $userUuid
            , ':docEstimateUuid'      => $docEstimateUuid
            , ':docEmails'            => $emails
            , ':mailSendOrderRelUuid' => $mailSendOrderRelUuid
        ]);
        $stmt = $this->adapter->createStatement(self::$selectInto, $params);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param string $docEstimateSendUuid
     * @return array
     */
    public function getDocEstimateSend(string $docEstimateSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_estimate_send_uuid' => $docEstimateSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
