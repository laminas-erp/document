<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocProformaTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_proforma';
    protected string $viewDocProforma = 'view_doc_proforma';

    public function getDocProformaMini(string $docProformaUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_proforma_uuid' => $docProformaUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocProforma(string $docProformaUuid): bool
    {
        return !empty($this->getDocProformaMini($docProformaUuid));
    }

    /**
     * @param string $docProformaUuid
     * @return array
     */
    public function getDocProforma(string $docProformaUuid): array
    {
        $select = new Select($this->viewDocProforma);
        try {
            $select->where(['doc_proforma_uuid' => $docProformaUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocProforma(string $filename, int $docProformaNo, string $docProformaNoCompl, string $orderUuid, string $userUuid, float $price, float $tax): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_proforma_uuid'     => $uuid,
                'doc_proforma_filename' => $filename,
                'doc_proforma_no'       => $docProformaNo,
                'doc_proforma_no_compl' => $docProformaNoCompl,
                'order_uuid'            => $orderUuid,
                'user_uuid_create'      => $userUuid,
                'doc_proforma_price'    => $price,
                'doc_proforma_tax'      => $tax,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocProforma(string $docProformaUuid, string $filename, string $userUuid, float $price, float $tax): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_proforma_time_replace' => new Expression('current_timestamp'),
                'doc_proforma_filename'     => $filename,
                'user_uuid_replace'         => $userUuid,
                'doc_proforma_price'        => $price,
                'doc_proforma_tax'          => $tax,
            ]);
            $update->where(['doc_proforma_uuid' => $docProformaUuid]);
            if ($this->updateWith($update) > 0) {
                return $docProformaUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocProformaNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_proforma_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocProformaNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_proforma_no)')]);
            $select->where->greaterThanOrEqualTo('doc_proforma_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_proforma_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getDocProformasForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocProforma);
        try {
            $select->order('doc_proforma_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
