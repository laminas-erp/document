<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocDeliveryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_delivery';
    protected string $viewDocDelivery = 'view_doc_delivery';

    /**
     * @param string $docDeliveryUuid
     * @return array
     */
    public function getDocDeliveryMini(string $docDeliveryUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_delivery_uuid' => $docDeliveryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocDelivery(string $docDeliveryUuid): bool
    {
        return !empty($this->getDocDeliveryMini($docDeliveryUuid));
    }

    /**
     * @param string $docDeliveryUuid
     * @return array From db.view_doc_delivery
     */
    public function getDocDelivery(string $docDeliveryUuid): array
    {
        $select = new Select($this->viewDocDelivery);
        try {
            $select->where(['doc_delivery_uuid' => $docDeliveryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocDelivery(string $filename, int $docDeliveryNo, string $docDeliveryNoCompl, string $orderUuid, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_delivery_uuid'     => $uuid,
                'doc_delivery_filename' => $filename,
                'doc_delivery_no'       => $docDeliveryNo,
                'doc_delivery_no_compl' => $docDeliveryNoCompl,
                'order_uuid'            => $orderUuid,
                'user_uuid_create'      => $userUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocDelivery(string $docDeliveryUuid, string $filename, string $userUuid): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_delivery_time_replace' => new Expression('current_timestamp'),
                'doc_delivery_filename'     => $filename,
                'user_uuid_replace'         => $userUuid,
            ]);
            $update->where(['doc_delivery_uuid' => $docDeliveryUuid]);
            if ($this->updateWith($update) > 0) {
                return $docDeliveryUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxDocDeliveryNo(): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_delivery_no)')]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getMaxDocDeliveryNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(doc_delivery_no)')]);
            $select->where->greaterThanOrEqualTo('doc_delivery_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('doc_delivery_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_doc_delivery
     */
    public function getDocDeliverysForOrder(string $orderUuid): array
    {
        $select = new Select($this->viewDocDelivery);
        try {
            $select->order('doc_delivery_time_create DESC');
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
