<?php

namespace Lerp\Document\Table\Order;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Order\Entity\Order\OrderItemEntity;

class DocInvoiceItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_invoice_item';

    /**
     * @param string $docInvoiceItemUuid
     * @return array
     */
    public function getDocInvoiceItem(string $docInvoiceItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_invoice_item_uuid' => $docInvoiceItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDocInvoiceItemsByOrderItemUuid(string $orderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteDocInvoiceItemsForDocInvoiceUuid(string $docInvoiceUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_invoice_uuid' => $docInvoiceUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocInvoiceItem(string $docInvoiceUuid, OrderItemEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_invoice_item_uuid'  => $uuid,
                'doc_invoice_uuid'       => $docInvoiceUuid,
                'order_item_uuid'        => $entity->getUuid(),
                'order_item_text_short'  => $entity->getOrderItemTextShort(),
                'order_item_text_long'   => $entity->getOrderItemTextLong(),
                'order_item_quantity'    => $entity->getOrderItemQuantity(),
                'quantityunit_uuid'      => $entity->getQuantityunitUuid(),
                'order_item_price'       => $entity->getOrderItemPrice(),
                'order_item_price_total' => $entity->getOrderItemPriceTotal(),
                'order_item_taxp'        => $entity->getOrderItemTaxp(),
                'cost_centre_id'         => $entity->getCostCentreId(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $docInvoiceUuid
     * @param bool $asAssoc
     * @return array From `view_doc_invoice_item`.
     */
    public function getDocInvoiceItemsByDocInvoiceUuid(string $docInvoiceUuid, bool $asAssoc = true): array
    {
        $select = new Select('view_doc_invoice_item');
        try {
            $select->where(['doc_invoice_uuid' => $docInvoiceUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($asAssoc) {
                    $assoc = [];
                    do {
                        $current = $result->current();
                        $assoc[$current['order_item_uuid']] = $current;
                        $result->next();
                    } while ($result->valid());
                    return $assoc;
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array From `view_doc_invoice_item`.
     */
    public function getDocInvoiceItemsForProduct(string $productUuid): array
    {
        $select = new Select('view_doc_invoice_item');
        try {
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
