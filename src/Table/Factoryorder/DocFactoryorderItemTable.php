<?php

namespace Lerp\Document\Table\Factoryorder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Factoryorder\Entity\FactoryorderEntity;
use Lerp\Factoryorder\Entity\FactoryorderWorkflowEntity;

class DocFactoryorderItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_factoryorder_item';

    /**
     * @param string $docFactoryorderItemUuid
     * @return array
     */
    public function getDocFactoryorderItem(string $docFactoryorderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_factoryorder_item_uuid' => $docFactoryorderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDocFactoryorderItemsByFactoryorderWorkflowUuid(string $factoryorderWorkflowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['factoryorder_workflow_uuid' => $factoryorderWorkflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteDocFactoryorderItemsForDocFactoryorderUuid(string $docFactoryorderUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_factoryorder_uuid' => $docFactoryorderUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDocFactoryorderItem(string $docFactoryorderUuid, FactoryorderWorkflowEntity $entity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_factoryorder_item_uuid' => $uuid,
                'doc_factoryorder_uuid' => $docFactoryorderUuid,
                'factoryorder_workflow_uuid' => $entity->getUuid(),
                'factoryorder_workflow_time' => $entity->getFactoryorderWorkflowTime(),
                'factoryorder_workflow_price_per_hour' => $entity->getFactoryorderWorkflowPricePerHour(),
                'factoryorder_workflow_text' => $entity->getFactoryorderWorkflowText(),
                'factoryorder_workflow_order_priority' => $entity->getFactoryorderWorkflowOrderPriority(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
