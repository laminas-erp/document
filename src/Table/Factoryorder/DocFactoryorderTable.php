<?php

namespace Lerp\Document\Table\Factoryorder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DocFactoryorderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'doc_factoryorder';

    /**
     * @param string $docFactoryorderUuid The doc_factoryorder_uuid is UNIQUE in the table.
     * @return array
     */
    public function getDocFactoryorderMini(string $docFactoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['doc_factoryorder_uuid' => $docFactoryorderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDocFactoryorder(string $docFactoryorderUuid): bool
    {
        return !empty($this->getDocFactoryorderMini($docFactoryorderUuid));
    }

    protected array $viewFactoryorderJoinColumns = [
        'factoryorder_no', 'product_uuid', 'order_item_uuid', 'factoryorder_briefing', 'factoryorder_quantity',
        'factoryorder_time_create',
        'factoryorder_time_update',
        'factoryorder_time_create_unix',
        'order_no', 'order_no_compl', 'product_text_part', 'product_text_short', 'product_no_no', 'count_workflow'];

    protected function makeJoinDefault(Select $select): void
    {
        $select->join('view_factoryorder', 'view_factoryorder.factoryorder_uuid = doc_factoryorder.factoryorder_uuid'
            , $this->viewFactoryorderJoinColumns
            , Select::JOIN_LEFT);
    }

    /**
     * Data from db.doc_factoryorder JOIN view_factoryorder.
     * @param string $docFactoryorderUuid
     * @return array
     */
    public function getDocFactoryorder(string $docFactoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->where(['doc_factoryorder_uuid' => $docFactoryorderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDocFactoryorderByFactoryorderUuid(string $factoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->where(['doc_factoryorder.factoryorder_uuid' => $factoryorderUuid]);
            $q = $select->getSqlString($this->getAdapter()->getPlatform());
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDocFactoryorder(string $filename, string $factoryorderUuid, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'doc_factoryorder_uuid' => $uuid,
                'doc_factoryorder_filename' => $filename,
                'factoryorder_uuid' => $factoryorderUuid,
                'user_uuid_create' => $userUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDocFactoryorder(string $docFactoryorderUuid, string $filename, string $userUuid): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'doc_factoryorder_time_replace' => new Expression('current_timestamp'),
                'doc_factoryorder_filename' => $filename,
                'user_uuid_replace' => $userUuid,
            ]);
            $update->where(['doc_factoryorder_uuid' => $docFactoryorderUuid]);
            if ($this->updateWith($update) > 0) {
                return $docFactoryorderUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getDocFactoryordersForOrderItem(string $orderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->order('doc_factoryorder_time_create DESC');
            $select->where(['order_item_uuid' => $orderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteDocFactoryorder(string $docFactoryorderUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['doc_factoryorder_uuid' => $docFactoryorderUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
