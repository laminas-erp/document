<?php

namespace Lerp\Document\Entity\Stream;

class DocOutput
{
    const CONTENTTYPE_APP_PDF = 'application/pdf';
    const CONTENTDISPO_ATTACHMENT = 'attachment';
    const CONTENTDISPO_INLINE = 'inline';
    protected string $filePath;
    protected string $outFilename;
    protected string $contentType = self::CONTENTTYPE_APP_PDF;
    protected string $contentDispo = self::CONTENTDISPO_ATTACHMENT;

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getOutFilename(): string
    {
        return $this->outFilename;
    }

    public function setOutFilename(string $outFilename): void
    {
        $this->outFilename = $outFilename;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): void
    {
        $this->contentType = $contentType;
    }

    public function getContentDispo(): string
    {
        return $this->contentDispo;
    }

    public function setContentDispo(string $contentDispo): void
    {
        $this->contentDispo = $contentDispo;
    }

}
