<?php

namespace Lerp\Document\Entity;

/**
 * Class BaseDataEntity
 * @package Lerp\Document\Entity
 * Holds data for all PDFs.
 */
class BaseDataEntity
{
    const INFOBOX_OURSIGN = 'our_sign';
    const INFOBOX_OURCONTACT = 'our_contact';
    const INFOBOX_YOURSIGN = 'your_sign';
    const INFOBOX_YOURCONTACT = 'your_contact';
    const INFOBOX_OFFER_NO = 'offer_no';
    const INFOBOX_ORDER_NO = 'order_no';
    const INFOBOX_ORDERCONFIRM_NO = 'orderconfirm_no';
    const INFOBOX_INVOICE_NO = 'invoice_no';
    const INFOBOX_PURCHASEORDER_NO = 'purchaseorder_no';
    const INFOBOX_PURCHASEREQUEST_NO = 'purchaserequest_no';
    protected array $infobox = [
        self::INFOBOX_OURSIGN => '',
        self::INFOBOX_OURCONTACT => '',
        self::INFOBOX_YOURSIGN => '',
        self::INFOBOX_YOURCONTACT => '',
        self::INFOBOX_OFFER_NO => '',
        self::INFOBOX_ORDER_NO => '',
        self::INFOBOX_ORDERCONFIRM_NO => '',
        self::INFOBOX_INVOICE_NO => '',
        self::INFOBOX_PURCHASEORDER_NO => '',
        self::INFOBOX_PURCHASEREQUEST_NO => '',
    ];
    protected string $langISO = '';
    protected string $recipientLine1 = '';
    protected string $recipientLine2 = '';
    protected string $recipientLine3 = '';
    protected string $recipientLine4 = '';
    protected string $salutation = '';
    protected string $contentStartHTML = '';
    protected string $contentEndHTML = '';

    /**
     * @return string[]
     */
    public function getInfobox(): array
    {
        return $this->infobox;
    }

    /**
     * @param string $key One of the INFOBOX_* constants in BaseDataEntity (self).
     * @return string
     * E.g. you iterate over the $this->infobox array. Then you can fetch the value of the array key here.
     */
    public function getInfoboxValue(string $key): string
    {
        if (!isset($this->infobox[$key])) {
            return '';
        }
        return $this->infobox[$key];
    }

    /**
     * @return array All four recipient lines.
     */
    public function getRecipientLines(): array
    {
        return [$this->recipientLine1, $this->recipientLine2, $this->recipientLine3, $this->recipientLine4];
    }

    public function setRecipientLines(array $recipientLines): void
    {
        if (count($recipientLines) != 4) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() $addressLine needs exact four elements: line1, line2, line3, line4');
        }
        $this->recipientLine1 = $recipientLines[0];
        $this->recipientLine2 = $recipientLines[1];
        $this->recipientLine3 = $recipientLines[2];
        $this->recipientLine4 = $recipientLines[3];
    }

    public function getLangISO(): string
    {
        return $this->langISO;
    }

    public function setLangISO(string $langISO): void
    {
        $this->langISO = $langISO;
    }

    public function getRecipientLine1(): string
    {
        return $this->recipientLine1;
    }

    public function setRecipientLine1(string $recipientLine1): void
    {
        $this->recipientLine1 = $recipientLine1;
    }

    public function getRecipientLine2(): string
    {
        return $this->recipientLine2;
    }

    public function setRecipientLine2(string $recipientLine2): void
    {
        $this->recipientLine2 = $recipientLine2;
    }

    public function getRecipientLine3(): string
    {
        return $this->recipientLine3;
    }

    public function setRecipientLine3(string $recipientLine3): void
    {
        $this->recipientLine3 = $recipientLine3;
    }

    public function getRecipientLine4(): string
    {
        return $this->recipientLine4;
    }

    public function setRecipientLine4(string $recipientLine4): void
    {
        $this->recipientLine4 = $recipientLine4;
    }

    public function getInfoboxOurSign(): string
    {
        return $this->infobox[self::INFOBOX_OURSIGN];
    }

    public function setInfoboxOurSign(string $value): void
    {
        $this->infobox[self::INFOBOX_OURSIGN] = $value;
    }

    public function getInfoboxOurContact(): string
    {
        return $this->infobox[self::INFOBOX_OURCONTACT];
    }

    public function setInfoboxOurContact(string $value): void
    {
        $this->infobox[self::INFOBOX_OURCONTACT] = $value;
    }

    public function getInfoboxYourSign(): string
    {
        return $this->infobox[self::INFOBOX_YOURSIGN];
    }

    public function setInfoboxYourSign(string $value): void
    {
        $this->infobox[self::INFOBOX_YOURSIGN] = $value;
    }

    public function getInfoboxYourContact(): string
    {
        return $this->infobox[self::INFOBOX_YOURCONTACT];
    }

    public function setInfoboxYourContact(string $value): void
    {
        $this->infobox[self::INFOBOX_YOURCONTACT] = $value;
    }

    public function getInfoboxOfferNo(): string
    {
        return $this->infobox[self::INFOBOX_OFFER_NO];
    }

    public function setInfoboxOfferNo(string $value): void
    {
        $this->infobox[self::INFOBOX_OFFER_NO] = $value;
    }

    public function getInfoboxOrderNo(): string
    {
        return $this->infobox[self::INFOBOX_ORDER_NO];
    }

    public function setInfoboxOrderNo(string $value): void
    {
        $this->infobox[self::INFOBOX_ORDER_NO] = $value;
    }

    public function getInfoboxOrderConfirmNo(): string
    {
        return $this->infobox[self::INFOBOX_ORDERCONFIRM_NO];
    }

    public function setInfoboxOrderConfirmNo(string $value): void
    {
        $this->infobox[self::INFOBOX_ORDERCONFIRM_NO] = $value;
    }

    public function getInfoboxInvoiceNo(): string
    {
        return $this->infobox[self::INFOBOX_INVOICE_NO];
    }

    public function setInfoboxInvoiceNo(string $value): void
    {
        $this->infobox[self::INFOBOX_INVOICE_NO] = $value;
    }

    public function getInfoboxPurchaseOrderNo(): string
    {
        return $this->infobox[self::INFOBOX_PURCHASEORDER_NO];
    }

    public function setInfoboxPurchaseOrderNo(string $value): void
    {
        $this->infobox[self::INFOBOX_PURCHASEORDER_NO] = $value;
    }

    public function setInfoboxPurchaseRequestNo(string $value): void
    {
        $this->infobox[self::INFOBOX_PURCHASEREQUEST_NO] = $value;
    }

    public function getSalutation(): string
    {
        return $this->salutation;
    }

    public function setSalutation(string $salutation): void
    {
        $this->salutation = $salutation;
    }

    public function getContentStartHTML(): string
    {
        return html_entity_decode($this->contentStartHTML);
    }

    public function setContentStartHTML(string $contentStartHTML): void
    {
        $this->contentStartHTML = $contentStartHTML;
    }

    /**
     * @return string Decoded HTML content
     */
    public function getContentEndHTML(): string
    {
        return html_entity_decode($this->contentEndHTML);
    }

    public function setContentEndHTML(string $contentEndHTML): void
    {
        $this->contentEndHTML = $contentEndHTML;
    }

}
